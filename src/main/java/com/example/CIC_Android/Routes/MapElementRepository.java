package main.java.com.example.CIC_Android.Routes;

import main.java.com.example.CIC_Android.interfaces.IMapRepository;
import main.java.com.example.CIC_Android.interfaces.IRepositoryListener;
import org.mapsforge.android.maps.overlay.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 01.10.13
 * Time: 10:27
 */
public class MapElementRepository implements IMapRepository {

    MapElementRepository instance = null;

    List<IRepositoryListener> repositoryListenerses = new ArrayList<IRepositoryListener>();

    private MapElementRepository() {

    }

    public MapElementRepository getInstance() {
        if (instance == null)
            instance = new MapElementRepository();
        return instance;
    }

    private ArrayItemizedOverlay arrayItemizedOverlay;

    private ArrayCircleOverlay arrayCircleOverlay;

    private ArrayWayOverlay arrayWayOverlay;

    public ArrayItemizedOverlay getArrayItemizedOverlay() {
        return arrayItemizedOverlay;
    }

    public ArrayCircleOverlay getArrayCircleOverlay() {
        return arrayCircleOverlay;
    }

    public ArrayWayOverlay getArrayWayOverlay() {
        return arrayWayOverlay;
    }

    @Override
    public void addOverlayWay(OverlayWay wayOverlay) {
        if (getArrayWayOverlay()!=null)
            getArrayWayOverlay().addWay(wayOverlay);
    }

    @Override
    public void addOverlayWay(OverlayCircle overlayCircle) {
        if (getArrayCircleOverlay()!=null)
            getArrayCircleOverlay().addCircle(overlayCircle);
    }

    @Override
    public void addOverlayItem(OverlayItem overlayItem) {
        if (getArrayItemizedOverlay()!=null)
            getArrayItemizedOverlay().addItem(overlayItem);
    }
    //Listeners

    public void addRepositoryListener(IRepositoryListener repositoryListener) {
        if (repositoryListener != null)
            this.repositoryListenerses.add(repositoryListener);
        else throw new IllegalArgumentException("repositoryListenerses = null");
    }

    public void removeListener() {
        if (repositoryListenerses != null)
            this.repositoryListenerses.clear();
        else throw new IllegalArgumentException("repositoryListenerses = null");
    }

    ////////CLEAR REPOSITORY

    @Override
    public void clearRepository() {
        arrayCircleOverlay.clear();
        arrayItemizedOverlay.clear();
        arrayWayOverlay.clear();
    }

    @Override
    public List<Object> getOverlays() {
        ArrayList<Object> overlays = new ArrayList<Object>();
        overlays.add(arrayCircleOverlay);
        overlays.add(arrayItemizedOverlay);
        overlays.add(arrayWayOverlay);

        return overlays;
    }




}
