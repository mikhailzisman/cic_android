package main.java.com.example.CIC_Android.Routes;

import android.graphics.Paint;
import org.mapsforge.android.maps.overlay.*;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 01.10.13
 * Time: 17:27
 */
public class MapElementsBuilder {

    public static OverlayItem getOverlayItem(){
        return new OverlayItem();
    }

    public static WayOverlay getWayOverlay(Paint defaultPaintFill, Paint defaultPaintOutline, final int size, final int way){
        return new WayOverlay(defaultPaintFill, defaultPaintOutline) {
            @Override
            public int size() {
                return size;
            }

            @Override
            protected OverlayWay createWay(int i) {
                return new OverlayWay();
            }
        };

    }

    public static OverlayCircle CircleOverlay(Paint paintFill, Paint paintOutline){
        return new OverlayCircle(paintFill, paintOutline);
    }
}
