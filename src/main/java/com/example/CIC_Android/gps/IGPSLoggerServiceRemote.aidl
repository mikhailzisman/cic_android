package main.java.com.example.CIC_Android.gps;

import android.net.Uri;
import android.location.Location;

interface IGPSLoggerServiceRemote {

	int loggingState();
    long startLogging();
    void pauseLogging();
    long resumeLogging();
	void stopLogging();
    boolean isMediaPrepared();
    void storeDerivedDataSource(in String sourceName);
    Location getLastWaypoint();
    float getTrackedDistance();
}