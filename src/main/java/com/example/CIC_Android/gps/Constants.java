/*------------------------------------------------------------------------------
 **     Ident: Sogeti Smart Mobile Solutions
 **    Author: rene
 ** Copyright: (c) Apr 24, 2011 Sogeti Nederland B.V. All Rights Reserved.
 **------------------------------------------------------------------------------
 ** Sogeti Nederland B.V.            |  No part of this file may be reproduced  
 ** Distributed Software Engineering |  or transmitted in any form or by any        
 ** Lange Dreef 17                   |  means, electronic or mechanical, for the      
 ** 4131 NJ Vianen                   |  purpose, without the express written    
 ** The Netherlands                  |  permission of the copyright holder.
 *------------------------------------------------------------------------------
 *
 *   This file is part of OpenGPSTracker.
 *
 *   OpenGPSTracker is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   OpenGPSTracker is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with OpenGPSTracker.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package main.java.com.example.CIC_Android.gps;

import android.content.Context;
import android.os.Environment;
import android.preference.PreferenceManager;

import java.io.File;

public class Constants
{

   public static final String SPEEDSANITYCHECK = "speedsanitycheck";
   public static final String PRECISION = "precision";
   public static final String LOGATSTARTUP = "logatstartup";

   public static final String SERVICENAME = "main.java.com.example.CIC_Android.gps.GPSLoggerService";

   public static final String SDDIR_DIR             = "SDDIR_DIR";
   public static final String DEFAULT_EXTERNAL_DIR  = "/OpenGPSTracker/";
   public static final String TMPICTUREFILE_SUBPATH = "media_tmp.tmp";

   public static final String LOGGING_INTERVAL = "customprecisiontime";
   public static final String LOGGING_DISTANCE = "customprecisiondistance";
   public static final String STATUS_MONITOR = "gpsstatusmonitor";
   
   /**
    * Broadcast intent action indicating that the logger service state has
    * changed. Includes the logging state and its precision.
    *
    * @see #EXTRA_LOGGING_PRECISION
    * @see #EXTRA_LOGGING_STATE
    */

   public static final String LOGGING_STATE_CHANGED_ACTION = "nl.sogeti.android.gpstracker.LOGGING_STATE_CHANGED";
   
   /**
    * The precision the service is logging on.
    * 
    * @see #LOGGING_FINE
    * @see #LOGGING_NORMAL
    * @see #LOGGING_COARSE
    * @see #LOGGING_GLOBAL
    * @see #LOGGING_CUSTOM
    * 
    */
   public static final String EXTRA_LOGGING_PRECISION = "nl.sogeti.android.gpstracker.EXTRA_LOGGING_PRECISION";
   
   /**
    * The state the service is.
    * 
    * @see #UNKNOWN
    * @see #LOGGING
    * @see #PAUSED
    * @see #STOPPED
    */
   public static final String EXTRA_LOGGING_STATE = "nl.sogeti.android.gpstracker.EXTRA_LOGGING_STATE";

   /**
    * The state of the service is unknown
    */
   public static final int UNKNOWN = -1;
   
   /**
    * The service is actively logging, it has requested location update from the location provider.
    */
   public static final int LOGGING = 1;
   
   /**
    * The service is not active, but can be resumed to become active and store location changes as 
    * part of a new segment of the current track.
    */
   public static final int PAUSED = 2;
   
   /**
    * The service is not active and can not resume a current track but must start a new one when becoming active.
    */
   public static final int STOPPED = 3;
   
   /**
    * The precision of the GPS provider is based on the custom time interval and distance.
    */
   public static final int LOGGING_CUSTOM = 0;
   
   /**
    * The GPS location provider is asked to update every 10 seconds or every 5 meters.
    */
   public static final int LOGGING_FINE   = 1;
   
   /**
    * The GPS location provider is asked to update every 15 seconds or every 10 meters.
    */
   public static final int LOGGING_NORMAL = 2;
   
   /**
    * The GPS location provider is asked to update every 30 seconds or every 25 meters.
    */
   public static final int LOGGING_COARSE = 3;
   
   /**
    * The radio location provider is asked to update every 5 minutes or every 500 meters.
    */
   public static final int LOGGING_GLOBAL = 4;
    public static final int MAP_ACTIVITY_INDEX = 0;
            ;

    /**
    * Based on preference return the SD-Card directory in which Open GPS Tracker creates and stores files
    * shared tracks,
    * 
    * @param ctx
    * @return 
    */
   public static String getSdCardDirectory( Context ctx )
   {
      // Read preference and ensure start and end with '/' symbol
      String dir = PreferenceManager.getDefaultSharedPreferences(ctx).getString(SDDIR_DIR, DEFAULT_EXTERNAL_DIR);
      if( !dir.startsWith("/") )
      {
         dir = "/" + dir;
      }
      if( !dir.endsWith("/") )
      {
         dir = dir + "/" ;
      }
      dir = Environment.getExternalStorageDirectory().getAbsolutePath() + dir;
      
      // If neither exists or can be created fall back to default
      File dirHandle = new File(dir);
      if( !dirHandle.exists() && !dirHandle.mkdirs() )
      {
         dir = Environment.getExternalStorageDirectory().getAbsolutePath() + DEFAULT_EXTERNAL_DIR;
      }
      return dir;
   }
   
   public static String getSdCardTmpFile( Context ctx )
   {
      String dir = getSdCardDirectory( ctx ) + TMPICTUREFILE_SUBPATH;
      return dir;
   }

   public static final String BROADCAST_STREAM = "STREAM_ENABLED";


}
