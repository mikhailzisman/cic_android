package main.java.com.example.CIC_Android.gps;

import android.location.Location;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.json.JSONArray;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 11.09.13
 * Time: 16:58
 */

@DatabaseTable
public class Waypoint {

    @DatabaseField(generatedId = true)
    private int id;
    /** The latitude */
    @DatabaseField(dataType = DataType.DOUBLE)
    private double latitude;
    /** The longitude */
    @DatabaseField(dataType = DataType.DOUBLE)
    private double longitude;
    /** The recorded measurement_date */
    @DatabaseField(dataType = DataType.LONG)
    private long measurement_date;
    /** The speed in meters per second */
    @DatabaseField(dataType = DataType.DOUBLE)
    private double speed;
    /** The accuracy of the fix */
    @DatabaseField(dataType = DataType.DOUBLE, canBeNull = true)
    private double accuracy;
    /** The altitude */
    @DatabaseField(dataType = DataType.DOUBLE, canBeNull = true)
    private double altitude;
    /** the bearing of the fix */
    @DatabaseField(dataType = DataType.DOUBLE, canBeNull = true)
    private double bearing;

    @DatabaseField(dataType = DataType.STRING, canBeNull = true)
    private String device_type;

    @DatabaseField(dataType = DataType.LONG, canBeNull = false)
    private long car_id;


    public Waypoint() {
    }

    public Waypoint(Double latitude, Double longitude, Long time) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.measurement_date = time;
    }

    public Waypoint(Long car_id, String device_type, Double latitude, Double longitude, Long time, Double speed, Double accuracy, Double altitude, Double bearing) {
        this.car_id = car_id;
        this.device_type = device_type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.measurement_date = time;
        this.speed = speed;
        this.accuracy = accuracy;
        this.altitude = altitude;
        this.bearing = bearing;
    }

    public Waypoint(Location location) {
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.measurement_date = location.getTime();
        this.speed = location.getSpeed();
        this.accuracy = location.getAccuracy();
        this.altitude = location.getAltitude();
        this.bearing = location.getBearing();
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Long getMeasurement_date() {
        return measurement_date;
    }

    public void setMeasurement_date(Long measurement_date) {
        this.measurement_date = measurement_date;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getBearing() {
        return bearing;
    }

    public void setBearing(Double bearing) {
        this.bearing = bearing;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public long getCar_id() {
        return car_id;
    }

    public void setCar_id(long car_id) {
        this.car_id = car_id;
    }



    @Override
    public String toString() {
        return "Waypoint{" +
                "id=" + id +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", measurement_date=" + measurement_date +
                ", speed=" + speed +
                ", accuracy=" + accuracy +
                ", altitude=" + altitude +
                ", bearing=" + bearing +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Waypoint waypoint = (Waypoint) o;

        if (Double.compare(waypoint.accuracy, accuracy) != 0) return false;
        if (Double.compare(waypoint.altitude, altitude) != 0) return false;
        if (Double.compare(waypoint.bearing, bearing) != 0) return false;
        if (Double.compare(waypoint.latitude, latitude) != 0) return false;
        if (Double.compare(waypoint.longitude, longitude) != 0) return false;
        if (Double.compare(waypoint.speed, speed) != 0) return false;
        if (measurement_date != waypoint.measurement_date) return false;
        if (id != waypoint.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) (measurement_date ^ (measurement_date >>> 32));
        temp = Double.doubleToLongBits(speed);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(accuracy);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(altitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(bearing);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
