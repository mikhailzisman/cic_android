package main.java.com.example.CIC_Android.gps;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.*;
import android.location.GpsStatus.Listener;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.*;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.example.CIC_Android.R;
import com.octo.android.robospice.SpiceManager;
import main.java.com.example.CIC_Android.Application;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Semaphore;

public class GPSLoggerService extends Service implements LocationListener {
    private static final float FINE_DISTANCE = 5F;
    private static final long FINE_INTERVAL = 1000l;
    private static final float FINE_ACCURACY = 20f;

    private static final float NORMAL_DISTANCE = 10F;
    private static final long NORMAL_INTERVAL = 15000l;
    private static final float NORMAL_ACCURACY = 30f;

    private static final float COARSE_DISTANCE = 25F;
    private static final long COARSE_INTERVAL = 30000l;
    private static final float COARSE_ACCURACY = 75f;

    private static final float GLOBAL_DISTANCE = 500F;
    private static final long GLOBAL_INTERVAL = 300000l;
    private static final float GLOBAL_ACCURACY = 1000f;

    /**
     * <code>MAX_REASONABLE_SPEED</code> is about 324 kilometer per hour or 201
     * mile per hour.
     */
    private static final int MAX_REASONABLE_SPEED = 321;//90

    /**
     * <code>MAX_REASONABLE_ALTITUDECHANGE</code> between the last few waypoints
     * and a new one the difference should be less then 200 meter.
     */
    private static final int MAX_REASONABLE_ALTITUDECHANGE = 200;

    private static final boolean DEBUG = false;
    private static final boolean VERBOSE = false;
    private static final String TAG = "GPSLoggerService";

    private static final String SERVICESTATE_DISTANCE = "SERVICESTATE_DISTANCE";
    private static final String SERVICESTATE_STATE = "SERVICESTATE_STATE";
    private static final String SERVICESTATE_PRECISION = "SERVICESTATE_PRECISION";

    private static final int ADDGPSSTATUSLISTENER = 0;
    private static final int REQUEST_FINEGPS_LOCATIONUPDATES = 1;
    private static final int REQUEST_NORMALGPS_LOCATIONUPDATES = 2;
    private static final int REQUEST_COARSEGPS_LOCATIONUPDATES = 3;
    private static final int REQUEST_GLOBALNETWORK_LOCATIONUPDATES = 4;
    private static final int REQUEST_CUSTOMGPS_LOCATIONUPDATES = 5;
    private static final int STOPLOOPER = 6;
    private static final int GPSPROBLEM = 7;

    public static final String COMMAND = "nl.sogeti.android.gpstracker.extra.COMMAND";
    public static final int EXTRA_COMMAND_START = 0;
    public static final int EXTRA_COMMAND_PAUSE = 1;
    public static final int EXTRA_COMMAND_RESUME = 2;
    public static final int EXTRA_COMMAND_STOP = 3;

    private LocationManager mLocationManager;
    private PowerManager.WakeLock mWakeLock;
    private Handler mHandler;

    /**
     * If speeds should be checked to sane values
     */
    private boolean mSpeedSanityCheck;

    /**
     * If broadcasts of location about should be sent to stream location
     */
    private boolean mStreamBroadcast;

    private int mPrecision;
    private int mLoggingState = Constants.STOPPED;

    private Location mPreviousLocation;
    private float mDistance;

    private Vector<Location> mWeakLocations;
    private Queue<Double> mAltitudes;

    /**
     * <code>mAcceptableAccuracy</code> indicates the maximum acceptable accuracy
     * of a waypoint in meters.
     */
    private float mMaxAcceptableAccuracy = 20;

    private boolean mShowingGpsDisabled;

    /**
     * Should the GPS Status monitor update the notification bar
     */
    private boolean mStatusMonitor;

    /**
     * Time thread to runs tasks that check whether the GPS listener has received
     * enough to consider the GPS system alive.
     */
    private Timer mHeartbeatTimer;

    /**
     * Listens to changes in preference to precision and sanity checks
     */
    private OnSharedPreferenceChangeListener mSharedPreferenceChangeListener = new OnSharedPreferenceChangeListener() {

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals(Constants.PRECISION) || key.equals(Constants.LOGGING_DISTANCE) || key.equals(Constants.LOGGING_INTERVAL)) {
                sendRequestLocationUpdatesMessage();
                crashProtectState();

            } else if (key.equals(Constants.SPEEDSANITYCHECK)) {
                mSpeedSanityCheck = sharedPreferences.getBoolean(Constants.SPEEDSANITYCHECK, true);
            } else if (key.equals(Constants.STATUS_MONITOR)) {
                mLocationManager.removeGpsStatusListener(mStatusListener);
                sendRequestStatusUpdateMessage();

            } else if (key.equals(Constants.BROADCAST_STREAM) || key.equals("VOICEOVER_ENABLED") || key.equals("CUSTOMUPLOAD_ENABLED")) {
                if (key.equals(Constants.BROADCAST_STREAM)) {
                    mStreamBroadcast = sharedPreferences.getBoolean(Constants.BROADCAST_STREAM, false);
                }
            }
        }
    };


    @Override
    public void onLocationChanged(Location location) {
        if (VERBOSE) {
            Log.v(TAG, "onLocationChanged( Location " + location + " )");
        }

        Location filteredLocation = locationFilter(location);
        if (filteredLocation != null) {

            // Obey the start segment if the previous location is unknown or far away
            if (mPreviousLocation == null || filteredLocation.distanceTo(mPreviousLocation) > 4 * mMaxAcceptableAccuracy) {
                resetDistance();
            } else if (mPreviousLocation != null) {
                mDistance += mPreviousLocation.distanceTo(filteredLocation);
            }
            storeLocation(filteredLocation);
            mPreviousLocation = location;
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        if (DEBUG) {
            Log.d(TAG, "onProviderDisabled( String " + provider + " )");
        }

        if (mPrecision != Constants.LOGGING_GLOBAL && provider.equals(LocationManager.GPS_PROVIDER)) {
            notifyToast(R.string.service_gpsdisabled);
        } else if (mPrecision == Constants.LOGGING_GLOBAL && provider.equals(LocationManager.NETWORK_PROVIDER)) {
            notifyToast(R.string.service_datadisabled);
        }

    }

    private void notifyToast(int resource) {
        Toast.makeText(this, this.getString(resource), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
        if (DEBUG) {
            Log.d(TAG, "onProviderEnabled( String " + provider + " )");
        }

        if (mPrecision != Constants.LOGGING_GLOBAL && provider.equals(LocationManager.GPS_PROVIDER)) {
            notifyToast(R.string.service_gpsenabled);
        } else if (mPrecision == Constants.LOGGING_GLOBAL && provider.equals(LocationManager.NETWORK_PROVIDER)) {
            notifyToast(R.string.service_dataenabled);
        }


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        if (DEBUG) {
            Log.d(TAG, "onStatusChanged( String " + provider + ", int " + status + ", Bundle " + extras + " )");
        }

        if (status == LocationProvider.OUT_OF_SERVICE) {
            Log.e(TAG, String.format("Provider %s changed to status %d", provider, status));
        }
    }

    /**
     * Listens to GPS status changes
     */
    private Listener mStatusListener = new GpsStatus.Listener() {
        @Override
        public synchronized void onGpsStatusChanged(int event) {
            switch (event) {
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    if (mStatusMonitor) {
                        GpsStatus status = mLocationManager.getGpsStatus(null);
                        Iterable<GpsSatellite> list = status.getSatellites();
                        for (GpsSatellite satellite : list) {
                            if (satellite.usedInFix()) {
                            }
                        }
                    }
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                    break;
                case GpsStatus.GPS_EVENT_STARTED:
                    break;
                default:
                    break;
            }
        }
    };
    private IBinder mBinder = new IGPSLoggerServiceRemote.Stub() {
        @Override
        public int loggingState() throws RemoteException {
            return mLoggingState;
        }

        @Override
        public long startLogging() throws RemoteException {
            GPSLoggerService.this.startLogging();
            return 1;
        }

        @Override
        public void pauseLogging() throws RemoteException {
            GPSLoggerService.this.pauseLogging();
        }

        @Override
        public long resumeLogging() throws RemoteException {
            GPSLoggerService.this.resumeLogging();
            return 1;
        }

        @Override
        public void stopLogging() throws RemoteException {
            GPSLoggerService.this.stopLogging();
        }

        @Override
        public boolean isMediaPrepared() throws RemoteException {
            return GPSLoggerService.this.isMediaPrepared();
        }

        @Override
        public void storeDerivedDataSource(String sourceName) throws RemoteException {
            //GPSLoggerService.this.storeDerivedDataSource(sourceName);
        }

        @Override
        public Location getLastWaypoint() throws RemoteException {
            return GPSLoggerService.this.getLastWaypoint();
        }

        @Override
        public float getTrackedDistance() throws RemoteException {
            return GPSLoggerService.this.getTrackedDistance();
        }
    };

    /**
     * Task that will be run periodically during active logging to verify that
     * the logging really happens and that the GPS hasn't silently stopped.
     */
    private TimerTask mHeartbeat = null;

    /**
     * Task to determine if the GPS is alive
     */
    class Heartbeat extends TimerTask {

        private String mProvider;

        public Heartbeat(String provider) {
            mProvider = provider;
        }

        @Override
        public void run() {
            if (isLogging()) {
                // Collect the last location from the last logged location or a more recent from the last weak location
                Location checkLocation = mPreviousLocation;
                synchronized (mWeakLocations) {
                    if (!mWeakLocations.isEmpty()) {
                        if (checkLocation == null) {
                            checkLocation = mWeakLocations.lastElement();
                        } else {
                            Location weakLocation = mWeakLocations.lastElement();
                            //Get last of this
                            checkLocation = getLastOfLocations(checkLocation, weakLocation);
                        }
                    }
                }
                // Is the last known GPS location something nearby we are not told?
                Location managerLocation = mLocationManager.getLastKnownLocation(mProvider);
                if (managerLocation != null && checkLocation != null) {
                    if (checkLocation.distanceTo(managerLocation) < 2 * mMaxAcceptableAccuracy) {
                        checkLocation = getLastOfLocations(checkLocation, managerLocation);
                    }
                }

                if (checkLocation == null || checkLocation.getTime() + mCheckPeriod < new Date().getTime()) {
                    Log.w(TAG, "GPS system failed to produce a location during logging: " + checkLocation);
                    mLoggingState = Constants.PAUSED;
                    resumeLogging();

                    if (mStatusMonitor) {
                        soundGpsSignalAlarm();
                    }

                }
            }
        }

        private Location getLastOfLocations(Location location1, Location location2) {
            return location2.getTime() > location1.getTime() ? location2 : location1;
        }
    }



    /**
     * Number of milliseconds that a functioning GPS system needs to provide a
     * location. Calculated to be either 120 seconds or 4 times the requested
     * period, whichever is larger.
     */
    private long mCheckPeriod;

    private class GPSLoggerServiceThread extends Thread {
        public Semaphore ready = new Semaphore(0);

        GPSLoggerServiceThread() {
            this.setName("GPSLoggerServiceThread");
        }

        @Override
        public void run() {
            Looper.prepare();
            mHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    _handleMessage(msg);
                }
            };
            ready.release(); // Signal the looper and handler are created
            Looper.loop();
        }
    }

    /**
     * Called by the system when the service is first created. Do not call this
     * method directly. Be sure to call super.onCreate().
     */
    @Override
    public void onCreate() {
        super.onCreate();

        android.os.Debug.waitForDebugger();

        if (DEBUG) {
            Log.d(TAG, "onCreate()");
        }

        GPSLoggerServiceThread looper = new GPSLoggerServiceThread();
        looper.start();
        try {
            looper.ready.acquire();
        } catch (InterruptedException e) {
            Log.e(TAG, "Interrupted during wait for the GPSLoggerServiceThread to start, prepare for trouble!", e);
        }
        mHeartbeatTimer = new Timer("heartbeat", true);

        mWeakLocations = new Vector<Location>(3);
        mAltitudes = new LinkedList<Double>();
        mLoggingState = Constants.STOPPED;
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mSpeedSanityCheck = sharedPreferences.getBoolean(Constants.SPEEDSANITYCHECK, true);
        mStreamBroadcast = sharedPreferences.getBoolean(Constants.BROADCAST_STREAM, false);
        boolean startImmidiatly = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Constants.LOGATSTARTUP, false);

        crashRestoreState();
        if (startImmidiatly && mLoggingState == Constants.STOPPED) {
            startLogging();
        } else {
            //broadCastLoggingState();
        }
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleCommand(intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_NOT_STICKY;
    }

    private void handleCommand(Intent intent) {
        if (DEBUG) {
            Log.d(TAG, "handleCommand(Intent " + intent + ")");
        }

        if (intent != null && intent.hasExtra(COMMAND)) {
            switch (intent.getIntExtra(COMMAND, -1)) {
                case EXTRA_COMMAND_START:
                    startLogging();
                    break;
                case EXTRA_COMMAND_PAUSE:
                    pauseLogging();
                    break;
                case EXTRA_COMMAND_RESUME:
                    resumeLogging();
                    break;
                case EXTRA_COMMAND_STOP:
                    stopLogging();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onDestroy() {
        if (DEBUG) {
            Log.d(TAG, "onDestroy()");
        }

        super.onDestroy();

        if (isLogging()) {
            Log.w(TAG, "Destroyin an activly logging service");
        }
        mHeartbeatTimer.cancel();
        mHeartbeatTimer.purge();
        if (this.mWakeLock != null) {
            this.mWakeLock.release();
            this.mWakeLock = null;
        }
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this.mSharedPreferenceChangeListener);
        mLocationManager.removeGpsStatusListener(mStatusListener);
        stopListening();

        Message msg = Message.obtain();
        msg.what = STOPLOOPER;
        mHandler.sendMessage(msg);
    }

    private void crashProtectState() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Editor editor = preferences.edit();
        editor.putInt(SERVICESTATE_PRECISION, mPrecision);
        editor.putInt(SERVICESTATE_STATE, mLoggingState);
        editor.putFloat(SERVICESTATE_DISTANCE, mDistance);
        editor.commit();
        if (DEBUG) {
            Log.d(TAG, "crashProtectState()");
        }

    }

    private synchronized void crashRestoreState() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        long previousState = preferences.getInt(SERVICESTATE_STATE, Constants.STOPPED);
        if (previousState == Constants.LOGGING || previousState == Constants.PAUSED) {
            Log.w(TAG, "Recovering from a crash or kill and restoring state.");

            mPrecision = preferences.getInt(SERVICESTATE_PRECISION, -1);
            mDistance = preferences.getFloat(SERVICESTATE_DISTANCE, 0F);
            if (previousState == Constants.LOGGING) {
                mLoggingState = Constants.PAUSED;
                resumeLogging();
            } else if (previousState == Constants.PAUSED) {
                mLoggingState = Constants.LOGGING;
                pauseLogging();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    protected boolean isLogging() {
        return this.mLoggingState == Constants.LOGGING;
    }

    protected Location getLastWaypoint() {
        Location myLastWaypoint = null;
        if (isLogging()) {
            myLastWaypoint = mPreviousLocation;
        }
        return myLastWaypoint;
    }

    public float getTrackedDistance() {
        float distance = 0F;
        if (isLogging()) {
            distance = mDistance;
        }
        return distance;
    }

    protected boolean isMediaPrepared() {
        return true;
        //return !(mTrackId < 0 || mSegmentId < 0 || mWaypointId < 0);
    }

    public synchronized void startLogging() {
        if (DEBUG) {
            Log.d(TAG, "startLogging()");
        }

        if (this.mLoggingState == Constants.STOPPED) {
            resetDistance();
            sendRequestLocationUpdatesMessage();
            sendRequestStatusUpdateMessage();
            this.mLoggingState = Constants.LOGGING;
            updateWakeLock();
            crashProtectState();
        }
    }

    public synchronized void pauseLogging() {
        if (DEBUG) {
            Log.d(TAG, "pauseLogging()");
        }

        if (this.mLoggingState == Constants.LOGGING) {
            mLocationManager.removeGpsStatusListener(mStatusListener);
            stopListening();
            mLoggingState = Constants.PAUSED;
            mPreviousLocation = null;
            updateWakeLock();
            crashProtectState();
        }
    }

    public synchronized void resumeLogging() {
        if (DEBUG) {
            Log.d(TAG, "resumeLogging()");
        }

        if (this.mLoggingState == Constants.PAUSED) {

            sendRequestLocationUpdatesMessage();
            sendRequestStatusUpdateMessage();

            this.mLoggingState = Constants.LOGGING;
            updateWakeLock();

            crashProtectState();
        }
    }

    public synchronized void stopLogging() {
        if (DEBUG) {
            Log.d(TAG, "stopLogging()");
        }

        mLoggingState = Constants.STOPPED;
        crashProtectState();

        updateWakeLock();

        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this.mSharedPreferenceChangeListener);

        mLocationManager.removeGpsStatusListener(mStatusListener);
        stopListening();

    }

    private void startListening(String provider, long intervaltime, float distance) {
        mLocationManager.removeUpdates(this);
        if (mLocationManager.isProviderEnabled(provider))
            mLocationManager.requestLocationUpdates(provider, intervaltime, distance, this);
        mCheckPeriod = Math.max(12 * intervaltime, 120 * 1000);
        if (mHeartbeat != null) {
            mHeartbeat.cancel();
            mHeartbeat = null;
        }
        mHeartbeat = new Heartbeat(provider);
        mHeartbeatTimer.schedule(mHeartbeat, mCheckPeriod, mCheckPeriod);
    }

    private void stopListening() {
        if (mHeartbeat != null) {
            mHeartbeat.cancel();
            mHeartbeat = null;
        }
        mLocationManager.removeUpdates(this);
    }

    private void sendRequestStatusUpdateMessage() {
        mStatusMonitor = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Constants.STATUS_MONITOR, false);
        Message msg = Message.obtain();
        msg.what = ADDGPSSTATUSLISTENER;
        mHandler.sendMessage(msg);
    }

    private void sendRequestLocationUpdatesMessage() {
        stopListening();
        mPrecision = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.PRECISION, "2")).intValue();
        Message msg = Message.obtain();
        switch (mPrecision) {
            case (Constants.LOGGING_FINE): // Fine
                msg.what = REQUEST_FINEGPS_LOCATIONUPDATES;
                mHandler.sendMessage(msg);
                break;
            case (Constants.LOGGING_NORMAL): // Normal
                msg.what = REQUEST_NORMALGPS_LOCATIONUPDATES;
                mHandler.sendMessage(msg);
                break;
            case (Constants.LOGGING_COARSE): // Coarse
                msg.what = REQUEST_COARSEGPS_LOCATIONUPDATES;
                mHandler.sendMessage(msg);
                break;
            case (Constants.LOGGING_GLOBAL): // Global
                msg.what = REQUEST_GLOBALNETWORK_LOCATIONUPDATES;
                mHandler.sendMessage(msg);
                break;
            case (Constants.LOGGING_CUSTOM): // Global
                msg.what = REQUEST_CUSTOMGPS_LOCATIONUPDATES;
                mHandler.sendMessage(msg);
                break;
            default:
                Log.e(TAG, "Unknown precision " + mPrecision);
                break;
        }
    }

    /**
     * Message handler method to do the work off-loaded by mHandler to
     * GPSLoggerServiceThread
     *
     * @param msg
     */
    private void _handleMessage(Message msg) {
        if (DEBUG) {
            Log.d(TAG, "_handleMessage( Message " + msg + " )");
        }

        long intervaltime = 0;
        float distance = 0;
        switch (msg.what) {
            case ADDGPSSTATUSLISTENER:
                this.mLocationManager.addGpsStatusListener(mStatusListener);
                break;
            case REQUEST_FINEGPS_LOCATIONUPDATES:
                mMaxAcceptableAccuracy = FINE_ACCURACY;
                intervaltime = FINE_INTERVAL;
                distance = FINE_DISTANCE;
                startListening(LocationManager.GPS_PROVIDER, intervaltime, distance);
                break;
            case REQUEST_NORMALGPS_LOCATIONUPDATES:
                mMaxAcceptableAccuracy = NORMAL_ACCURACY;
                intervaltime = NORMAL_INTERVAL;
                distance = NORMAL_DISTANCE;
                startListening(LocationManager.GPS_PROVIDER, intervaltime, distance);
                break;
            case REQUEST_COARSEGPS_LOCATIONUPDATES:
                mMaxAcceptableAccuracy = COARSE_ACCURACY;
                intervaltime = COARSE_INTERVAL;
                distance = COARSE_DISTANCE;
                startListening(LocationManager.GPS_PROVIDER, intervaltime, distance);
                break;
            case REQUEST_GLOBALNETWORK_LOCATIONUPDATES:
                mMaxAcceptableAccuracy = GLOBAL_ACCURACY;
                intervaltime = GLOBAL_INTERVAL;
                distance = GLOBAL_DISTANCE;
                startListening(LocationManager.NETWORK_PROVIDER, intervaltime, distance);
                if (!isNetworkConnected()) {
                    notifyToast(R.string.service_connectiondisabled);
                }
                break;
            case REQUEST_CUSTOMGPS_LOCATIONUPDATES:
                intervaltime = 60 * 1000 * Long.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.LOGGING_INTERVAL, "15000"));
                distance = Float.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.LOGGING_DISTANCE, "10"));
                mMaxAcceptableAccuracy = Math.max(10f, Math.min(distance, 50f));
                startListening(LocationManager.GPS_PROVIDER, intervaltime, distance);
                break;
            case STOPLOOPER:
                mLocationManager.removeGpsStatusListener(mStatusListener);
                stopListening();
                Looper.myLooper().quit();
                break;
            case GPSPROBLEM:
                notifyToast(R.string.service_gpsproblem);
                break;
        }
    }

    private void updateWakeLock() {
        if (this.mLoggingState == Constants.LOGGING) {
            PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(mSharedPreferenceChangeListener);

            PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
            if (this.mWakeLock != null) {
                this.mWakeLock.release();
                this.mWakeLock = null;
            }
            this.mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
            this.mWakeLock.acquire();
        } else {
            if (this.mWakeLock != null) {
                this.mWakeLock.release();
                this.mWakeLock = null;
            }
        }
    }

    /**
     * Some GPS waypoints received are of to low a quality for tracking use. Here
     * we filter those out.
     *
     * @param proposedLocation
     * @return either the (cleaned) original or null when unacceptable
     */
    public Location locationFilter(Location proposedLocation) {
        // Do no include log wrong 0.0 lat 0.0 long, skip to next value in while-loop

        if (proposedLocation != null && (proposedLocation.getLatitude() == 0.0d || proposedLocation.getLongitude() == 0.0d)) {
            Log.w(TAG, "A wrong location was received, 0.0 latitude and 0.0 longitude... ");
            proposedLocation = null;
        }

        // Do not log a waypoint which is more inaccurate then is configured to be acceptable
        if (proposedLocation != null && proposedLocation.getAccuracy() > mMaxAcceptableAccuracy) {
            Log.w(TAG, String.format("A weak location was received, lots of inaccuracy... (%f is more then max %f)", proposedLocation.getAccuracy(),
                    mMaxAcceptableAccuracy));
            proposedLocation = addBadLocation(proposedLocation);
        }

        // Do not log a waypoint which might be on any side of the previous waypoint
        if (proposedLocation != null && mPreviousLocation != null && proposedLocation.getAccuracy() > mPreviousLocation.distanceTo(proposedLocation)) {
            Log.w(TAG,
                    String.format("A weak location was received, not quite clear from the previous waypoint... (%f more then max %f)",
                            proposedLocation.getAccuracy(), mPreviousLocation.distanceTo(proposedLocation)));
            proposedLocation = addBadLocation(proposedLocation);
        }

        // Speed checks, check if the proposed location could be reached from the previous one in sane speed
        // Common to jump on network logging and sometimes jumps on Samsung Galaxy S type of devices
        if (mSpeedSanityCheck && proposedLocation != null && mPreviousLocation != null) {
            // To avoid near instant teleportation on network location or glitches cause continent hopping
            float meters = proposedLocation.distanceTo(mPreviousLocation);
            long seconds = (proposedLocation.getTime() - mPreviousLocation.getTime()) / 1000L;
            float speed = meters / seconds;
            if (speed > MAX_REASONABLE_SPEED) {
                Log.w(TAG, "A strange location was received, a really high speed of " + speed + " m/s, prob wrong...");
                proposedLocation = addBadLocation(proposedLocation);
                // Might be a messed up Samsung Galaxy S GPS, reset the logging
                if (speed > 2 * MAX_REASONABLE_SPEED && mPrecision != Constants.LOGGING_GLOBAL) {
                    Log.w(TAG, "A strange location was received on GPS, reset the GPS listeners");
                    stopListening();
                    mLocationManager.removeGpsStatusListener(mStatusListener);
                    mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
                    sendRequestStatusUpdateMessage();
                    sendRequestLocationUpdatesMessage();
                }
            }
        }

        // Remove speed if not sane
        if (mSpeedSanityCheck && proposedLocation != null && proposedLocation.getSpeed() > MAX_REASONABLE_SPEED) {
            Log.w(TAG, "A strange speed, a really high speed, prob wrong...");
            proposedLocation.removeSpeed();
        }

        // Remove altitude if not sane
        if (mSpeedSanityCheck && proposedLocation != null && proposedLocation.hasAltitude()) {
            if (!addSaneAltitude(proposedLocation.getAltitude())) {
                Log.w(TAG, "A strange altitude, a really big difference, prob wrong...");
                proposedLocation.removeAltitude();
            }
        }
        // Older bad locations will not be needed
        if (proposedLocation != null) {
            mWeakLocations.clear();
        }
        return proposedLocation;
    }

    /**
     * Store a bad location, when to many bad locations are stored the the
     * storage is cleared and the least bad one is returned
     *
     * @param location bad location
     * @return null when the bad location is stored or the least bad one if the
     *         storage was full
     */
    private Location addBadLocation(Location location) {
        mWeakLocations.add(location);
        if (mWeakLocations.size() < 3) {
            location = null;
        } else {
            Location best = mWeakLocations.lastElement();
            for (Location whimp : mWeakLocations) {
                if (whimp.hasAccuracy() && best.hasAccuracy() && whimp.getAccuracy() < best.getAccuracy()) {
                    best = whimp;
                } else {
                    if (whimp.hasAccuracy() && !best.hasAccuracy()) {
                        best = whimp;
                    }
                }
            }
            synchronized (mWeakLocations) {
                mWeakLocations.clear();
            }
            location = best;
        }
        return location;
    }

    /**
     * Builds a bit of knowledge about altitudes to expect and return if the
     * added value is deemed sane.
     *
     * @param altitude
     * @return whether the altitude is considered sane
     */
    private boolean addSaneAltitude(double altitude) {
        boolean sane;
        double avg = 0;
        int elements = 0;
        // Even insane altitude shifts increases alter perception
        mAltitudes.add(altitude);
        if (mAltitudes.size() > 3) {
            mAltitudes.poll();
        }
        for (Double alt : mAltitudes) {
            avg += alt;
            elements++;
        }
        avg = avg / elements;
        sane = Math.abs(altitude - avg) < MAX_REASONABLE_ALTITUDECHANGE;

        return sane;
    }

    /**
     * Trigged by events that start a new track
     */
    private void resetDistance() {
        mDistance = 0;
    }


    /**
     * Use the ContentResolver mechanism to store a received location
     *
     * @param location
     */
    public void storeLocation(Location location) {
        if (!isLogging()) {
            Log.e(TAG, String.format("Not logging but storing location %s, prepare to fail", location.toString()));
        }


        Waypoint waypoint = new Waypoint();

        waypoint.setLatitude(location.getLatitude());
        waypoint.setLongitude(location.getLongitude());
        waypoint.setMeasurement_date(System.currentTimeMillis());
        waypoint.setSpeed((double) location.getSpeed());
        waypoint.setDevice_type("tablet");

        if (location.hasAccuracy()) {
            waypoint.setAccuracy((double) location.getAccuracy());
        }
        if (location.hasAltitude()) {
            waypoint.setAltitude(location.getAltitude());
        }
        if (location.hasBearing()) {
            waypoint.setBearing((double) location.getBearing());
        }
        //Save to database
        saveWaypointToDatabase(waypoint);
    }

    private void saveWaypointToDatabase(Waypoint waypoint) {
        getSpiceManager().putInCache(waypoint.hashCode(), waypoint);

        Log.d(TAG, "Waypoint saved: " + waypoint);
    }


    private boolean isNetworkConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connMgr.getActiveNetworkInfo();

        return (info != null && info.isConnected());
    }

    private void soundGpsSignalAlarm() {
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null) {
            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (alert == null) {
                alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        MediaPlayer mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(GPSLoggerService.this, alert);
            final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Problem setting data source for mediaplayer", e);
        } catch (SecurityException e) {
            Log.e(TAG, "Problem setting data source for mediaplayer", e);
        } catch (IllegalStateException e) {
            Log.e(TAG, "Problem with mediaplayer", e);
        } catch (IOException e) {
            Log.e(TAG, "Problem with mediaplayer", e);
        }
        Message msg = Message.obtain();
        msg.what = GPSPROBLEM;
        mHandler.sendMessage(msg);
    }

    private SpiceManager getSpiceManager() {
        return Application.getSpiceManager();
    }
}