package main.java.com.example.CIC_Android.gps;

import android.util.Log;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 18.09.13
 * Time: 2:06
 */
public class WaypointDAO extends BaseDaoImpl<Waypoint,Integer> {

    private static final String TAG = "WaypointDAO";

    protected WaypointDAO(Class<Waypoint> dataClass) throws SQLException {
        super(dataClass);
    }

    public Waypoint getLastWaypoint(){
        QueryBuilder<Waypoint,Integer> queryBuilder = queryBuilder();
        try {
            return queryBuilder.orderBy("time",false)
                   .queryForFirst();
        } catch (SQLException e) {
            Log.e(TAG, "Database reading error");
            e.printStackTrace();
        }
        return null;
    }
}
