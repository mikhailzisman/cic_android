package main.java.com.example.CIC_Android.gps;

import android.location.Location;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 11.09.13
 * Time: 16:16
 */
public interface ICheckLocationRule {

    boolean check(Location proposedLocation);
}
