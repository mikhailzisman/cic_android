package main.java.com.example.CIC_Android.model;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.09.13
 * Time: 16:40
 */
public class Order {

    private int id;

    private String addressFrom;

    private String addressTo;

    private String price;

    public Order() {
    }

    public Order(int id, String addressFrom, String addressTo, String price) {
        this.id = id;
        this.addressFrom = addressFrom;
        this.addressTo = addressTo;
        this.price = price;
    }

    public String getAddressFrom() {
        return addressFrom;
    }

    public void setAddressFrom(String addressFrom) {
        this.addressFrom = addressFrom;
    }

    public String getAddressTo() {
        return addressTo;
    }

    public void setAddressTo(String addressTo) {
        this.addressTo = addressTo;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String orderToHtml() {


//        String textForOutput = "<b> <font color=#979797> BTC "
//                + MtGoxClient.ResultArray[getReasonAlertID()] + "</font> </b>";
//
//        textForOutput = textForOutput.replace("EUR","€");
        //return textForOutput;
        return "";
    }
}
