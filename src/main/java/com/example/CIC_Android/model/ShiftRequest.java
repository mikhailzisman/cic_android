package main.java.com.example.CIC_Android.model;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import main.java.com.example.CIC_Android.Utils.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 30.08.13
 * Time: 23:26
 */
public class ShiftRequest extends SpringAndroidSpiceRequest<ShiftResponse> {
    private static final Logger logger = LoggerFactory.getLogger(LoginRequest.class);
    private String url;
    private User userForChangeShift;
    private TypeOfShift shift;
    private String tabletID;

    public ShiftRequest(Class<ShiftResponse> clazz, User userForChangeShift, TypeOfShift shift, String TabletID) {
        super(clazz);
        this.shift = shift;
        tabletID = TabletID;
        this.userForChangeShift = userForChangeShift;
    }


    @Override
    public ShiftResponse loadDataFromNetwork()  throws RestClientException {

        //http://cloudycab.sim-tech.ru/drivers/worktimes/shift.json?identity=driver&type=open&tablet=f970e2767d0cfe75876ea857f92e319b

        url = Settings.ServerDomain + "/drivers/worktimes/shift.json";

        // Create and populate a object to be used in the request
        Message message = new Message(userForChangeShift.getUser(),tabletID,shift.toString());

        ResponseEntity<ShiftResponse> responseEntity =  getRestTemplate().postForEntity(url, message, ShiftResponse.class);

        logger.debug(url+"\n"
                +"Status code:"+responseEntity.getStatusCode().toString()+"\n"
                +"Response body:"+responseEntity.getBody().toString());


        return responseEntity.getBody();
    }

//    @JsonAutoDetect
    private class Message
    {
        private String identity ;
        private String tablet;
        private String type;

        private Message(){

        }

        private Message(String identity, String tablet,String type) {
            this.identity = identity;
            this.type = type;
            this.tablet = tablet;
        }

        public String getIdentity() {
            return identity;
        }

        public void setIdentity(String identity) {
            this.identity = identity;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTablet() {
            return tablet;
        }

        public void setTablet(String tablet) {
            this.tablet = tablet;
        }
    }

}
