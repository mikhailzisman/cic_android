package main.java.com.example.CIC_Android.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 28.08.13
 * Time: 4:02
 */
@JsonAutoDetect
public class IsAuthorizedResponse {

    private static final Logger logger = LoggerFactory.getLogger(LoginResponse.class);

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean success;

    public IsAuthorizedResponse() {
    }

    public boolean isSuccess() {
        return success;
    }
}