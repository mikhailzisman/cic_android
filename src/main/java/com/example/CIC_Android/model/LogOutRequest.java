package main.java.com.example.CIC_Android.model;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import main.java.com.example.CIC_Android.Utils.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 28.08.13
 * Time: 3:53
 */
public class LogOutRequest extends SpringAndroidSpiceRequest<LogOutResponse> {
    private static final Logger logger = LoggerFactory.getLogger(ShiftResponse.class);
    public LogOutRequest(Class<LogOutResponse> clazz) {
        super(clazz);
    }

    @Override
    public LogOutResponse loadDataFromNetwork() throws Exception {
        String url = Settings.ServerDomain + "/users/logout.json";

        ResponseEntity<LogOutResponse> responseEntity =  getRestTemplate().getForEntity(url, LogOutResponse.class);

        logger.debug(url+"\n"+
                "Status code:"+responseEntity.getStatusCode().toString()+"\n"+
                "Response body:"+responseEntity.getBody().toString());


        return responseEntity.getBody();
    }
}
