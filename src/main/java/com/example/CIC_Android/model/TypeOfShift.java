package main.java.com.example.CIC_Android.model;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 30.08.13
 * Time: 23:28
 */
public enum TypeOfShift {
    open, close
}
