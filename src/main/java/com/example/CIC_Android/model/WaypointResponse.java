package main.java.com.example.CIC_Android.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonIgnoreType;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 20.09.13
 * Time: 12:30
 */
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class WaypointResponse {

    @DatabaseField(generatedId = true)
    private int id;

    public WaypointResponse() {
    }

}
