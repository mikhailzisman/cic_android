package main.java.com.example.CIC_Android.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.08.13
 * Time: 18:16
 */
public class User {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.STRING)
    private String user;

    @DatabaseField(dataType = DataType.STRING)
    private String password;

    @DatabaseField(dataType = DataType.STRING)
    private String auth_key;

    @DatabaseField(dataType = DataType.DATE_TIME)
    private Date expiration_date;

    public User() {

    }

    public User(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuth_key() {
        return auth_key;
    }

    public void setAuth_key(String auth_key) {
        this.auth_key = auth_key;
    }

    public Date getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                ", auth_key='" + auth_key + '\'' +
                ", expiration_date=" + expiration_date +
                '}';
    }
}
