package main.java.com.example.CIC_Android.model;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import main.java.com.example.CIC_Android.Utils.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.08.13
 * Time: 19:17
 */
public class LoginRequest extends SpringAndroidSpiceRequest<LoginResponse> {
    private static final Logger logger = LoggerFactory.getLogger(LoginRequest.class);
    private String url;
    private User userForRegistration;

    public LoginRequest(Class<LoginResponse> clazz, User userForRegistration) {
        super(clazz);
        this.userForRegistration = userForRegistration;
    }


    @Override
    public LoginResponse loadDataFromNetwork()  throws RestClientException {

        if (checkUser(userForRegistration))
            throw new IllegalArgumentException("Login or password are wrong");

        url = Settings.ServerDomain + "/users/auth.json"+"?"
                +"login="+userForRegistration.getUser()+"&"
                +"password="+userForRegistration.getPassword();

        ResponseEntity<LoginResponse> responseEntity =  getRestTemplate().getForEntity(url, LoginResponse.class);

        logger.debug(url+"\n"+
                "Status code:"+responseEntity.getStatusCode().toString()+"\n"+
                "Response body:"+responseEntity.getBody().toString());


        return responseEntity.getBody();
    }

    private boolean checkUser(User userForRegistration) {
        return (userForRegistration.getPassword().length()==0||
                userForRegistration.getUser().length()==0);

    }
}