package main.java.com.example.CIC_Android.model;

import com.j256.ormlite.field.DatabaseField;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 30.08.13
 * Time: 23:26
 */
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShiftResponse {

    @DatabaseField(generatedId = true)
    private int id;

    public ShiftResponse() {
    }

}