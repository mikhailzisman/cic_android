package main.java.com.example.CIC_Android.model;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import main.java.com.example.CIC_Android.Utils.Settings;
import main.java.com.example.CIC_Android.gps.Waypoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 20.09.13
 * Time: 12:30
 */
public class WaypointRequest extends SpringAndroidSpiceRequest<String> {

    private static final Logger logger = LoggerFactory.getLogger(WaypointRequest.class);

    private User user;
    private List<Waypoint> waypointList;

    public WaypointRequest(Class clazz, User user, List<Waypoint> waypointList) {
        super(clazz);
        this.user = user;
        this.waypointList = waypointList;
    }

    @Override
    public String loadDataFromNetwork()  throws RestClientException {

        String url = Settings.ServerDomain + "/cars/coordinates/";

        logger.debug("Sending request to tranfer Waypoint:");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        headers.add("data","data");
        HttpEntity<String> entity = new HttpEntity<String>(generateBodyFromWaypointList(waypointList), headers);

        ResponseEntity<String> responseEntity =  getRestTemplate().postForEntity(url, entity, String.class);

        logger.debug(url+"\n"
                +"Status code:"+responseEntity.getStatusCode().toString()+"\n"
                +"Response body:"+responseEntity.getBody().toString());

        return responseEntity.getBody();

    }

    private String generateBodyFromWaypointList(List<Waypoint> waypointList) {
        StringBuilder sb = new StringBuilder();
        int counter = 0;
        for (java.util.Iterator iterator = waypointList.iterator(); iterator.hasNext(); ) {
            Waypoint next =  (Waypoint)iterator.next();

            sb.append("data["+counter+"][device_type]="+next.getDevice_type() +
                    "&data["+counter+"][bearing]="+next.getBearing() +
                    "&data["+counter+"][accuracy]="+next.getAccuracy()+
                    "&data["+counter+"][altitude]="+next.getAltitude() +
                    "&data["+counter+"][latitude]="+next.getLatitude() +
                    "&data["+counter+"][longitude]="+next.getLongitude() +
                    "&data["+counter+"][measurement_date]="+next.getMeasurement_date() +
                    "&data["+counter+"][speed]="+next.getSpeed() +
                    "&data["+counter+"][car_id]="+next.getCar_id());

            counter++;

        }

        return sb.toString();

    }
}
