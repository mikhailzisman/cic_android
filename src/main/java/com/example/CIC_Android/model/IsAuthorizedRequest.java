package main.java.com.example.CIC_Android.model;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import main.java.com.example.CIC_Android.Utils.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 28.08.13
 * Time: 4:01
 */
public class IsAuthorizedRequest extends SpringAndroidSpiceRequest<IsAuthorizedResponse> {
    private static final Logger logger = LoggerFactory.getLogger(IsAuthorizedRequest.class);
    public IsAuthorizedRequest(Class<IsAuthorizedResponse> clazz) {
        super(clazz);
    }

    @Override
    public IsAuthorizedResponse loadDataFromNetwork() throws Exception {
        String url = Settings.ServerDomain + "/users/isauthorized.json";

        ResponseEntity<IsAuthorizedResponse> responseEntity =  getRestTemplate().getForEntity(url, IsAuthorizedResponse.class);

        logger.debug(url+"\n"+
                "Status code:"+responseEntity.getStatusCode().toString()+"\n"+
                "Response body:"+responseEntity.getBody().toString());


        return responseEntity.getBody();
    }
}
