package main.java.com.example.CIC_Android.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.08.13
 * Time: 18:56
 */
@JsonAutoDetect
public class LoginResponse {
    private static final Logger logger = LoggerFactory.getLogger(LoginResponse.class);

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean success;

    @JsonProperty(value = "Auth-Sid")
    @DatabaseField(dataType = DataType.STRING)
    private String auth_sid;

    @DatabaseField(dataType = DataType.INTEGER)
    private int user_id;

    public LoginResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public String getAuth_Sid() {
        return auth_sid;
    }

    public int getUser_id() {
        return user_id;
    }
}
