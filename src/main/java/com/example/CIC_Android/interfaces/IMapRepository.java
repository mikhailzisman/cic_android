package main.java.com.example.CIC_Android.interfaces;

import main.java.com.example.CIC_Android.gps.Route;
import org.mapsforge.android.maps.overlay.OverlayCircle;
import org.mapsforge.android.maps.overlay.OverlayItem;
import org.mapsforge.android.maps.overlay.OverlayWay;
import org.mapsforge.android.maps.overlay.WayOverlay;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 01.10.13
 * Time: 17:23
 */
public interface IMapRepository {

    void clearRepository();

    List<Object> getOverlays();

    public void addOverlayWay(OverlayWay overlayWay);

    void addOverlayWay(OverlayCircle overlayCircle);

    void addOverlayItem(OverlayItem overlayItem);
}
