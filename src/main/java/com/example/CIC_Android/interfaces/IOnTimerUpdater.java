package main.java.com.example.CIC_Android.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 18.09.13
 * Time: 10:11
 */
public interface IOnTimerUpdater {
    void update();
}
