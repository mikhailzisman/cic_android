package main.java.com.example.CIC_Android.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 01.10.13
 * Time: 18:15
 */
public interface IRepositoryListener {

    public void onRepositoryChanged();
}
