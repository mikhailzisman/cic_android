package main.java.com.example.CIC_Android.interfaces;

import main.java.com.example.CIC_Android.model.User;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 28.08.13
 * Time: 3:42
 */
public interface IAuth {

    public void login(String user, String password);

    public void logout();

    public void isLoggedRequest();

}
