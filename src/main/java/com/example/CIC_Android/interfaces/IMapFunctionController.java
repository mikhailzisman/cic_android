package main.java.com.example.CIC_Android.interfaces;

import main.java.com.example.CIC_Android.gps.Route;
import main.java.com.example.CIC_Android.gps.Waypoint;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 01.10.13
 * Time: 22:54
 */
public interface IMapFunctionController {

    public void addRoute(Route router);

    public void setCurrentPlaceMark(Waypoint waypoint);

    public void setCenter(Waypoint waypoint);
}
