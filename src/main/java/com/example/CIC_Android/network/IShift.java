package main.java.com.example.CIC_Android.network;

import main.java.com.example.CIC_Android.model.TypeOfShift;
import main.java.com.example.CIC_Android.model.User;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 30.08.13
 * Time: 23:49
 */
public interface IShift {

    void switchToShift(User user, TypeOfShift typeOfShift, String tabletID);

}
