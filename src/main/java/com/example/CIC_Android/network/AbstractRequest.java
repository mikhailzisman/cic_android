package main.java.com.example.CIC_Android.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractRequest {
    private static final Logger logger = LoggerFactory.getLogger(AuthorizerWorker.class);
    private Context context;
    private SpiceManager spiceManager;
    protected ParserError parserError;
    private ProgressDialog pd;

    private boolean progressDialogEnabled = true;

    public AbstractRequest(Context context, SpiceManager spiceManager, ParserError parserError) {
        this.context = context;
        this.spiceManager = spiceManager;
        this.parserError = parserError;
        pd = new ProgressDialog(context);
    }

    public void setProgressDialogOrToastEnabled(boolean progressDialogEnabled) {
        this.progressDialogEnabled = progressDialogEnabled;
    }

    public void showProgressDialog(String message) {
        if ((pd != null)&&progressDialogEnabled) {
            pd.setMessage(message);
            pd.show();
        }
    }

    public void stopProgressDialog() {
        if (pd != null) {
            pd.dismiss();
        }
    }

    public Context getContext() {
        return context;
    }

    public SpiceManager getSpiceManager() {
        return spiceManager;
    }

    public static Logger getLogger() {
        return logger;
    }

    public abstract void makeRequest(SpiceRequest request, RequestListener listener);

    public void NotifyOnSpiceException(SpiceException spiceException) {
        stopProgressDialog();
        String error = parserError.parseToString(spiceException);
        String message = error + ":" + spiceException.getLocalizedMessage();
        getLogger().error(message);
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
}