package main.java.com.example.CIC_Android.network;

import com.octo.android.robospice.persistence.exception.SpiceException;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 02.09.13
 * Time: 23:11
 */
public class ParserError {

    private static final Logger logger = LoggerFactory.getLogger(ParserError.class);

    private static String UnexpectedError = "Ошибка формата";

    private static String jsonError = "Ошибка формата";

    public ParserError() {
    }

    public String parseToString(SpiceException spiceException) {

        if (spiceException.getCause() instanceof HttpClientErrorException) {
            HttpClientErrorException exception = (HttpClientErrorException) spiceException.getCause();
            try {
                String jsonString = exception.getResponseBodyAsString();

                JSONObject error = new JSONObject(jsonString).getJSONArray("errors").getJSONObject(0);

                return error.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
                return jsonError;
            }

        }
        return UnexpectedError;
    }
}
