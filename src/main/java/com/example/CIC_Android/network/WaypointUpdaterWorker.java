package main.java.com.example.CIC_Android.network;

import android.content.Context;
import android.location.Location;
import android.widget.Toast;
import com.example.CIC_Android.R;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;
import main.java.com.example.CIC_Android.gps.GPSLoggerServiceManager;
import main.java.com.example.CIC_Android.gps.Waypoint;
import main.java.com.example.CIC_Android.interfaces.IOnTimerUpdater;
import main.java.com.example.CIC_Android.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 20.09.13
 * Time: 13:11
 */
public class WaypointUpdaterWorker extends AbstractRequest implements IOnTimerUpdater {

    private static final Logger logger = LoggerFactory.getLogger(WaypointUpdaterWorker.class);

    public WaypointUpdaterWorker(Context context, SpiceManager spiceManager,ParserError parserError) {
        super(context,spiceManager, parserError);
        setProgressDialogOrToastEnabled(false);
    }

    @Override
    public void update() {


        User user = AuthorizerWorker.getCurrentRegistredUser();
        if (user == null){
            logger.error("User is not defined");
            return;
        }
        Location lastLocation = getGPSLoggerServiceManager().getLastWaypoint();

        logger.debug("Last Location is: "+lastLocation);

        if (lastLocation==null) {
            logger.error("Location is not defined");
            return;
        }
        //Test, Should be changed!!!!!!!!!
        Waypoint currentWaypoint = new Waypoint(lastLocation);
        currentWaypoint.setDevice_type("tablet");
        currentWaypoint.setCar_id(1);
        //Should be changed!!!!!!!!!
        List<Waypoint> waypointList = new ArrayList<Waypoint>();
        waypointList.add(currentWaypoint);
        waypointList.add(currentWaypoint);

        logger.debug("Sending to server: "+user+"\n"+lastLocation);

        sendWaypointsToServer(user, waypointList);
    }

    public void sendWaypointsToServer(User user, List<Waypoint> waypointList){
        makeRequest(new WaypointRequest(WaypointResponse.class, user, waypointList), waypointResponseListener);
    }

    @Override
    public void makeRequest(SpiceRequest request, RequestListener listener) {
        showProgressDialog(getContext().getString(R.string.pleaseWait));

        if (getSpiceManager() != null)
            getSpiceManager().execute(request,
                    0,
                    DurationInMillis.ALWAYS_EXPIRED,
                    listener);
    }


    public GPSLoggerServiceManager getGPSLoggerServiceManager(){
        return GPSLoggerServiceManager.getInstance();
    }

    ///////////////////////////////////////////////LISTENERS/////////////////////////////////////////////

    private RequestListener<WaypointResponse> waypointResponseListener = new RequestListener<WaypointResponse>() {
        @Override
        public void onRequestFailure(SpiceException e) {
            NotifyOnSpiceException(e);
        }

        @Override
        public void onRequestSuccess(WaypointResponse waypointResponse) {
            getLogger().debug("Waypoint request is not succefull: " + waypointResponse.toString());
            String message = getContext().getString(R.string.waypoint_is_sended);
            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            stopProgressDialog();
        }
    };
}
