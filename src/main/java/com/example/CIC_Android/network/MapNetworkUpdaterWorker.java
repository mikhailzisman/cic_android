package main.java.com.example.CIC_Android.network;

import android.content.Context;
import com.example.CIC_Android.R;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;
import main.java.com.example.CIC_Android.fragments.MapFragment.IMapRepositoryController;
import main.java.com.example.CIC_Android.gps.Route;
import main.java.com.example.CIC_Android.interfaces.IMapFunctionController;
import main.java.com.example.CIC_Android.interfaces.IOnTimerUpdater;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 18.09.13
 * Time: 2:39
 */
public class MapNetworkUpdaterWorker extends AbstractRequest implements IOnTimerUpdater {

    IMapFunctionController mapRepositoryController;

    public MapNetworkUpdaterWorker(Context context, SpiceManager spiceManager, ParserError parserError) {
        super(context, spiceManager, parserError);

        setProgressDialogOrToastEnabled(false);
    }

    @Override
    public void update() {
        runRouteRequest();
    }

    public void setMapRepositoryController(IMapFunctionController mapRepositoryController) {
        this.mapRepositoryController = mapRepositoryController;
    }

    public IMapFunctionController getMapRepositoryController() {
        return mapRepositoryController;
    }

    public void runRouteRequest() {
        //makeRequest(new Route(Route.class), routeRequestListener);
    }

    @Override
    public void makeRequest(SpiceRequest request, RequestListener listener) {
        showProgressDialog(getContext().getString(R.string.pleaseWait));

        if (getSpiceManager() != null)
            getSpiceManager().execute(request,
                    0,
                    DurationInMillis.ALWAYS_EXPIRED,
                    listener);
    }

    ///////////////////////////////////////////////LISTENERS/////////////////////////////////////////////

    private RequestListener<Route> routeRequestListener = new RequestListener<Route>() {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
             NotifyOnSpiceException(spiceException);
        }

        @Override
        public void onRequestSuccess(Route route) {
            if (mapRepositoryController != null)
                mapRepositoryController.addRoute(route);

        }
    };


}
