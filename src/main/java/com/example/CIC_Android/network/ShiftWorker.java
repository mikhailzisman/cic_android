package main.java.com.example.CIC_Android.network;

import android.content.Context;
import android.widget.Toast;
import com.example.CIC_Android.R;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;
import main.java.com.example.CIC_Android.model.*;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 30.08.13
 * Time: 23:49
 */
public class ShiftWorker extends AbstractRequest implements IShift{



    public ShiftWorker(Context context, SpiceManager spiceManager, ParserError parserError) {
        super(context, spiceManager, parserError);
    }

    @Override
    public void makeRequest(SpiceRequest request, RequestListener listener) {
        showProgressDialog(getContext().getString(R.string.pleaseWait));

        if (getSpiceManager() != null)
            getSpiceManager().execute(request,
                    0,
                    DurationInMillis.ALWAYS_EXPIRED,
                    listener);
    }

    @Override
    public void switchToShift(User user, TypeOfShift typeOfShift, String tabletID) {

        makeRequest(new ShiftRequest(ShiftResponse.class, user, typeOfShift, tabletID),shiftListener);
    }

    /////////////////////////////////////LISTENERS/////////////////////////////////////////////////
    private RequestListener<ShiftResponse> shiftListener = new RequestListener<ShiftResponse>() {
        @Override
        public void onRequestFailure(SpiceException spiceException) {

            NotifyOnSpiceException(spiceException);
        }

        @Override
        public void onRequestSuccess(ShiftResponse shiftResponse) {

            stopProgressDialog();
        }
    };


}
