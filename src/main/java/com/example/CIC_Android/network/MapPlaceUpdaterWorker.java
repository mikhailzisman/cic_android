package main.java.com.example.CIC_Android.network;

import android.location.Location;
import main.java.com.example.CIC_Android.fragments.MapFragment.IMapRepositoryController;
import main.java.com.example.CIC_Android.gps.GPSLoggerServiceManager;
import main.java.com.example.CIC_Android.gps.Waypoint;
import main.java.com.example.CIC_Android.interfaces.IMapFunctionController;
import main.java.com.example.CIC_Android.interfaces.IOnTimerUpdater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 18.09.13
 * Time: 10:09
 */
public class MapPlaceUpdaterWorker implements IOnTimerUpdater {

    private static final Logger logger = LoggerFactory.getLogger(MapPlaceUpdaterWorker.class);

    private IMapFunctionController mapController;

    public IMapFunctionController getMapController() {
        return mapController;
    }

    public void setMapController(IMapFunctionController mapController) {
        this.mapController = mapController;
    }

    @Override
    public void update() {

        Location lastLocation = getGPSLoggerServiceManager().getLastWaypoint();

        logger.debug("Last Location is: "+lastLocation);

        if (lastLocation==null) return;

        Waypoint currentWaypoint = new Waypoint(lastLocation);

        showInMap(currentWaypoint);
    }

    private void showInMap(Waypoint currentWaypoint) {

        if (currentWaypoint == null) return;

        if (getMapController()==null){
            logger.error("mapRepositoryController == null. Map is not updated. Interrupted.");
            return;
        }

        getMapController().setCurrentPlaceMark(currentWaypoint);

        getMapController().setCenter(currentWaypoint);
    }


    public GPSLoggerServiceManager getGPSLoggerServiceManager(){
        return GPSLoggerServiceManager.getInstance();
    }
}
