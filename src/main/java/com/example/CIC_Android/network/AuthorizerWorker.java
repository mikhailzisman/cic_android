package main.java.com.example.CIC_Android.network;

import android.content.Context;
import android.widget.Toast;
import com.example.CIC_Android.R;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;
import main.java.com.example.CIC_Android.interfaces.IAuth;
import main.java.com.example.CIC_Android.model.*;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 28.08.13
 * Time: 3:45
 */
public class AuthorizerWorker extends AbstractRequest implements IAuth {

    private static boolean isLogged = false;
    private static User currentRegistredUser = null;
    private User tempUser;
    public User emptyUser = new User("","");

    public AuthorizerWorker(Context context, SpiceManager spiceManager,ParserError parserError) {
        super(context,spiceManager, parserError);
        currentRegistredUser = emptyUser;
    }

    public static User getCurrentRegistredUser() {
        return currentRegistredUser;
    }

    @Override
    public void login(String user, String password) {
        tempUser = new User(user, password);
        makeRequest(new LoginRequest(LoginResponse.class, tempUser), loginRequestListener);
    }

    @Override
    public void logout() {
        makeRequest(new LogOutRequest(LogOutResponse.class), logoutRequestListener);
    }

    @Override
    public void isLoggedRequest() {
        makeRequest(new IsAuthorizedRequest(IsAuthorizedResponse.class), isLoggedListener);
    }

    @Override
    public void makeRequest(SpiceRequest request, RequestListener listener) {
        showProgressDialog(getContext().getString(R.string.pleaseWait));

        if (getSpiceManager() != null)
            getSpiceManager().execute(request,
                    0,
                    DurationInMillis.ALWAYS_EXPIRED,
                    listener);
    }

    ///////////////////////////////////////////////LISTENERS/////////////////////////////////////////////

    private RequestListener<LoginResponse> loginRequestListener = new RequestListener<LoginResponse>() {
        @Override
        public void onRequestFailure(SpiceException e) {
            NotifyOnSpiceException(e);
        }

        @Override
        public void onRequestSuccess(LoginResponse loginResponse) {
            getLogger().debug("Login request is not succefull: " + loginResponse.toString());
            String message = getContext().getString(R.string.loggedSuccefully) + String.valueOf(loginResponse.isSuccess());
            Toast.makeText(getContext(), message,Toast.LENGTH_LONG).show();
            stopProgressDialog();
            currentRegistredUser = tempUser;
        }
    };


    private RequestListener<LogOutResponse> logoutRequestListener = new RequestListener<LogOutResponse>() {
        @Override
        public void onRequestFailure(SpiceException e) {
            NotifyOnSpiceException(e);
        }

        @Override
        public void onRequestSuccess(LogOutResponse logOutResponse) {
            String message = getContext().getString(R.string.logouted)+ String.valueOf(logOutResponse.isSuccess());
            Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
            stopProgressDialog();
        }

    };


    private RequestListener<IsAuthorizedResponse> isLoggedListener = new RequestListener<IsAuthorizedResponse>() {
        @Override
        public void onRequestFailure(SpiceException e) {
            NotifyOnSpiceException(e);
        }

        @Override
        public void onRequestSuccess(IsAuthorizedResponse isAuthorizedResponse) {
            isLogged = isAuthorizedResponse.isSuccess();
            String message = getContext().getString(R.string.loggedNow)+ String.valueOf(isAuthorizedResponse.isSuccess());
            Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
            stopProgressDialog();
        }

    };
}
