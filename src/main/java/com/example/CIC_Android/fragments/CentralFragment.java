package main.java.com.example.CIC_Android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.CIC_Android.R;
import main.java.com.example.CIC_Android.fragments.MapFragment.MainFragmentActivity;
import main.java.com.example.CIC_Android.fragments.MapFragment.MyMapFragment;
import main.java.com.example.CIC_Android.gps.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 13.09.13
 * Time: 1:11
 */
public class CentralFragment extends Fragment implements MainMenuFragment.MainMenuSelectedListener {
    private boolean mIsDualPane = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.empty_fragment, null);

    }

    @Override
    public void OnMenuSelected(int index) {

        if (mIsDualPane) {
                /* show as fragment */
            Fragment newFragment = getFragmentByIndex(index);

            if (newFragment != null)
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.empty_fragment_id, newFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null)
                        .commit();

        } else {
                /* start a separate activity */
            Intent intentToStartActivity = getIntentToStart(index);
            startActivity(intentToStartActivity);
        }
    }

    private Intent getIntentToStart(int index) {

        Class<?> activityClass = null;
        switch (index) {
            case Constants.MAP_ACTIVITY_INDEX:
                activityClass = MainFragmentActivity.class;
                break;
        }

        return new Intent(getActivity(), activityClass);
    }

    private Fragment getFragmentByIndex(int menuIndex) {

        Fragment fragment;
        switch (menuIndex) {
            case Constants.MAP_ACTIVITY_INDEX:
                fragment = MyMapFragment.getInstance();
                break;

            default:
                fragment = new EmptyFragment();
        }

        return fragment;
    }

}
