package main.java.com.example.CIC_Android.fragments.ControlPanelFragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.CIC_Android.R;
import main.java.com.example.CIC_Android.activities.OrdersListFragmentActivity;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 24.09.13
 * Time: 14:42
 */
public class ShiftControlFragmentButton extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.button_caption, container, false);

        TextView tvButton = (TextView)view.findViewById(R.id.textview);
        Drawable drawable = getResources().getDrawable(R.drawable.shift);

        tvButton.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        tvButton.setCompoundDrawablePadding(30);
        tvButton.setTextSize((float)30);
        tvButton.setText(getActivity().getString(R.string.shift));

        tvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), OrdersListFragmentActivity.class));
            }
        });
        return view;

    }
}
