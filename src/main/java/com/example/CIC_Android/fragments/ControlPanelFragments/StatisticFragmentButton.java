package main.java.com.example.CIC_Android.fragments.ControlPanelFragments;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.example.CIC_Android.R;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 24.09.13
 * Time: 14:00
 */
public class StatisticFragmentButton extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.button_caption, container, false);

        TextView tvButton = (TextView)view.findViewById(R.id.textview);
        Drawable drawable = getResources().getDrawable(R.drawable.statistics);

        tvButton.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        tvButton.setCompoundDrawablePadding(20);
        tvButton.setTextSize((float) 30);
        tvButton.setTextColor(Color.RED);
        tvButton.setText(getActivity().getString(R.string.statistics));
        return view;
    }


}
