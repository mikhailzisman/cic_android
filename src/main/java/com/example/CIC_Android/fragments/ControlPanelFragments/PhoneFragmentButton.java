package main.java.com.example.CIC_Android.fragments.ControlPanelFragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.example.CIC_Android.R;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 24.09.13
 * Time: 13:51
 */
public class PhoneFragmentButton extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.button_caption, container, false);

        TextView tvButton = (TextView)view.findViewById(R.id.textview);
        Drawable drawable = getResources().getDrawable(R.drawable.phone);

        tvButton.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
        tvButton.setCompoundDrawablePadding(30);
        tvButton.setTextSize((float)30);

        tvButton.setText(getActivity().getString(R.string.phone));
        return view;
    }


}
