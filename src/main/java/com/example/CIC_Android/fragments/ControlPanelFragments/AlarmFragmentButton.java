package main.java.com.example.CIC_Android.fragments.ControlPanelFragments;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.CIC_Android.R;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 26.09.13
 * Time: 15:46
 */
public class AlarmFragmentButton extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.button_caption, container, false);

        TextView tvButton = (TextView)view.findViewById(R.id.textview);
        Drawable drawable = getResources().getDrawable(R.drawable.exclamation_mark);

        tvButton.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
        tvButton.setCompoundDrawablePadding(30);
        tvButton.setTextSize((float)30);
        tvButton.setTextColor(Color.RED);
        tvButton.setText(getActivity().getString(R.string.alarm));
        return view;
    }


}
