package main.java.com.example.CIC_Android.fragments.OrderPanelFragments;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.widget.ListView;
import com.example.CIC_Android.R;
import main.java.com.example.CIC_Android.adapters.OrderItemArrayAdapter;
import main.java.com.example.CIC_Android.adapters.TempStaticRepository;
import main.java.com.example.CIC_Android.model.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.09.13
 * Time: 13:23
 */
public class OrdersListFragment extends ListFragment {

    List<Order> orderList;
    private OrderItemArrayAdapter adapter;

    static OrdersListFragment instance = null;

    static public OrdersListFragment getInstance() {
        if (instance == null)
            instance = new OrdersListFragment();

        return instance;
    }

    private OrdersListFragment() {

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        orderList = new ArrayList<Order>();
        adapter = new OrderItemArrayAdapter(getActivity(), 0, TempStaticRepository.orderList);

        ListView lv = getListView();
        ColorDrawable sage = new ColorDrawable(this.getResources().getColor(R.drawable.divider_color));
        lv.setDivider(sage);
        lv.setDividerHeight(1);
        setListAdapter(adapter);

    }


    @Override
    public void onResume() {
        super.onResume();

        update();
    }

    public void addOrder(Order order) {
        if (orderList != null)
            orderList.add(order);

        update();
    }


    public void clearOrder() {
        if (orderList != null)
            orderList.clear();

        update();
    }

    public void replaceWithListOrder(List<Order> newOrderList) {
        if (orderList != null)
            orderList = new ArrayList<Order>(newOrderList);
    }

    public void update() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }


    public List<Order> getOrderList() {
        return orderList;
    }
}
