package main.java.com.example.CIC_Android.fragments.OrderPanelFragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.CIC_Android.R;
import main.java.com.example.CIC_Android.fragments.MapFragment.MyMapFragment;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.09.13
 * Time: 13:22
 */
public class NavigatorFragmentButton extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.button_caption, container, false);

        TextView tvButton = (TextView)view.findViewById(R.id.textview);

        Drawable drawable = getResources().getDrawable(R.drawable.navigation);

        tvButton.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        tvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.order_list_layout, MyMapFragment.getInstance())
                        .commit();
            }
        });
        tvButton.setTextSize((float)20);
        tvButton.setText(getActivity().getString(R.string.navigator));
        return view;
    }
}
