package main.java.com.example.CIC_Android.fragments.OrderPanelFragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.CIC_Android.R;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.09.13
 * Time: 13:21
 */
public class BackFragmentButton extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.button_image, container, false);

        ImageView ivButton = (ImageView)view.findViewById(R.id.imageView);

        ivButton.setImageResource(R.drawable.back);

        return view;
    }
}
