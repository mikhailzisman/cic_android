package main.java.com.example.CIC_Android.fragments.OrderPanelFragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.CIC_Android.R;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.09.13
 * Time: 13:21
 */
public class ShiftFragmentButton extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.button_caption, container, false);

        TextView tvButton = (TextView)view.findViewById(R.id.textview);
        Drawable drawable = getResources().getDrawable(R.drawable.shift);

        tvButton.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        tvButton.setCompoundDrawablePadding(10);
        tvButton.setTextSize((float)30);
        tvButton.setText(getActivity().getString(R.string.shift));

        tvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.order_list_layout, OrdersListFragment.getInstance())
                        .commit();
            }


        });
        return view;
    }
}
