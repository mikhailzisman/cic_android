package main.java.com.example.CIC_Android.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.CIC_Android.R;
import main.java.com.example.CIC_Android.adapters.MenuItemArrayAdapter;
import main.java.com.example.CIC_Android.adapters.MenuItemButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 13.09.13
 * Time: 1:01
 */
public class MainMenuFragment extends Fragment {

    ListView mainMenuListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_layout, null);

        mainMenuListView = (ListView) view.findViewById(R.id.list);

        return view;
    }

    private ArrayList<MainMenuSelectedListener> mainMenuSelectedListenerArray = new ArrayList<MainMenuSelectedListener>();

    public void addListener(MainMenuSelectedListener mainMenuSelectedListener) {
        if (mainMenuSelectedListenerArray != null)
            mainMenuSelectedListenerArray.add(mainMenuSelectedListener);
    }

    public void cleaListener() {
        if (mainMenuSelectedListenerArray != null)
            mainMenuSelectedListenerArray.clear();
    }

    public interface MainMenuSelectedListener {

        public void OnMenuSelected(int index);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        List<MenuItemButton> menuItemButtonList = new ArrayList<MenuItemButton>();

        menuItemButtonList.add(new MenuItemButton(0, "Карта", null));

        menuItemButtonList.add(new MenuItemButton(1, "Еще что-то", null));

        MenuItemArrayAdapter adapter = new MenuItemArrayAdapter(getActivity(),
                0,
                menuItemButtonList);

        mainMenuListView.setAdapter(adapter);

        mainMenuListView.setOnItemClickListener(onItemClickListener);

        addListeners();

        adapter.notifyDataSetChanged();


    }

    private void addListeners() {

        MainMenuSelectedListener centralFragment =
                (MainMenuSelectedListener) getFragmentManager().findFragmentById(R.id.centralFragment);

        MainMenuSelectedListener titleFragment =
                (MainMenuSelectedListener) getFragmentManager().findFragmentById(R.id.titleFragment);

        addListener(centralFragment);

        addListener(titleFragment);
    }

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            if (mainMenuSelectedListenerArray == null)
                return;

            for (MainMenuSelectedListener menuSelectedListener : mainMenuSelectedListenerArray)
                menuSelectedListener.OnMenuSelected(position);
        }
    };

    @Override
    public void onDetach() {

        cleaListener();

        super.onDetach();
    }
}
