package main.java.com.example.CIC_Android.fragments.MapFragment;

import android.app.Activity;
import main.java.com.example.CIC_Android.gps.Route;
import main.java.com.example.CIC_Android.gps.Waypoint;
import main.java.com.example.CIC_Android.interfaces.IMapFunctionController;
import main.java.com.example.CIC_Android.interfaces.IMapRepository;
import org.mapsforge.core.model.GeoPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyMapFragment extends ActivityHostFragment implements IMapFunctionController {

    private static final Logger logger = LoggerFactory.getLogger(MyMapFragment.class);

    static MyMapFragment myMapFragment;

    public static MyMapFragment getInstance() {
        if (myMapFragment == null)
            myMapFragment = new MyMapFragment();
        return myMapFragment;
    }

    @Override
    protected Class<? extends Activity> getActivityClass() {
        return MyMapActivity.class;
    }

    private IMapRepositoryController getMapController() {

        Activity mapActivity = getHostedActivity();
        if (mapActivity instanceof IMapRepositoryController)
            return (IMapRepositoryController) mapActivity;

        return null;
    }

    private IMapRepository getMapRepository(){
        if (getMapController()!=null)
            return getMapController().getMapRepository();

        return null;
    }

    @Override
    public void addRoute(Route router) {
//         if (getMapRepository()!=null)
//             getMapRepository().addOverlayWay();
    }

    @Override
    public void setCurrentPlaceMark(Waypoint waypoint) {

    }

    @Override
    public void setCenter(Waypoint waypoint) {
        if ((getMapController()!=null)&&(getMapController().getMapView()!=null))
        getMapController()
                .getMapView()
                .setCenter(new GeoPoint(waypoint.getLatitude(),
                                        waypoint.getLongitude()));
        else logger.warn("mapView == null. May be it not been initialized");
    }
}
