package main.java.com.example.CIC_Android.fragments.MapFragment;

import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;
import main.java.com.example.CIC_Android.interfaces.IMapRepository;
import main.java.com.example.CIC_Android.interfaces.IRepositoryListener;
import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.overlay.ArrayCircleOverlay;
import org.mapsforge.android.maps.overlay.ArrayItemizedOverlay;
import org.mapsforge.android.maps.overlay.ArrayWayOverlay;
import org.mapsforge.android.maps.overlay.Overlay;
import org.mapsforge.map.reader.header.FileOpenResult;

import java.io.File;
import java.util.List;

public class MyMapActivity extends MapActivity implements IMapRepositoryController, IRepositoryListener {

    private static final File MAP_FILE = new File(Environment.getExternalStorageDirectory().getPath(), "russia.map");
    private MapView mapView = null;
    IMapRepository mapRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mapView = new MapView(this);
        mapView.setClickable(true);
        mapView.setBuiltInZoomControls(true);
        FileOpenResult fileOpenResult = mapView.setMapFile(MAP_FILE);
        if (!fileOpenResult.isSuccess()) {
            Toast.makeText(this, fileOpenResult.getErrorMessage(), Toast.LENGTH_LONG).show();
            finish();
        }
        setContentView(mapView);
    }

    @Override
    public void setMapRepository(IMapRepository mapRepository) {
        this.mapRepository = mapRepository;
    }

    @Override
    public IMapRepository getMapRepository() {
        return mapRepository;
    }

    @Override
    public MapView getMapView() {
        return mapView;
    }

    private List<Overlay> getCurrentOverlays() {
        if (mapView != null) {
            return mapView.getOverlays();
        }

        return null;
    }

    public void addOverlay(Overlay overlay) {
        if ((getCurrentOverlays() != null) &&
                !getCurrentOverlays().contains(overlay))
            getCurrentOverlays().add(overlay);
    }

    public void removeOverlay(Overlay overlay){
        if ((getCurrentOverlays() != null) &&
                getCurrentOverlays().contains(overlay))
            getCurrentOverlays().remove(overlay);
    }

    public void clearCurrentOverlay(){
        if (getCurrentOverlays() != null)
           getCurrentOverlays().clear();

    }

    public void recreateWithRepository(){


        clearCurrentOverlay();

        fillOverlayFromRepository();

    }

    private void fillOverlayFromRepository() {
        for (Object overlay: getMapRepository().getOverlays()){
            if (overlay instanceof ArrayItemizedOverlay){
                getCurrentOverlays().add((ArrayItemizedOverlay)overlay);
            }

            if (overlay instanceof ArrayWayOverlay){
                getCurrentOverlays().add((ArrayWayOverlay)overlay);
            }

            if (overlay instanceof ArrayCircleOverlay){
                getCurrentOverlays().add((ArrayCircleOverlay)overlay);
            }
        }
    }

    @Override
    public void onRepositoryChanged() {
        recreateWithRepository();
    }
}
