package main.java.com.example.CIC_Android.fragments.MapFragment;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.example.CIC_Android.R;

public class MainFragmentActivity extends FragmentActivity {
    
    @Override    
    protected void onCreate(Bundle bundle) {        
        super.onCreate(bundle);
        setContentView(R.layout.main_fragment_activity);
    }
}
