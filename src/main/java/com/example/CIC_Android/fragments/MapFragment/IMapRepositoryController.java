package main.java.com.example.CIC_Android.fragments.MapFragment;

import main.java.com.example.CIC_Android.interfaces.IMapRepository;
import org.mapsforge.android.maps.MapView;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 17.09.13
 * Time: 23:51
 */
public interface IMapRepositoryController {


    void setMapRepository(IMapRepository mapRepository);

    IMapRepository getMapRepository();

    MapView getMapView();


}
