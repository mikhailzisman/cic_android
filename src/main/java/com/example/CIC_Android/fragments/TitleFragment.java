package main.java.com.example.CIC_Android.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.CIC_Android.R;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 13.09.13
 * Time: 1:10
 */
public class TitleFragment extends Fragment implements MainMenuFragment.MainMenuSelectedListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.title_layout,null);

        return view;
    }

    @Override
    public void OnMenuSelected(int index) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
