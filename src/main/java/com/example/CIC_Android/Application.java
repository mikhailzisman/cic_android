package main.java.com.example.CIC_Android;

import android.content.Context;
import com.octo.android.robospice.SpiceManager;
import main.java.com.example.CIC_Android.Utils.BugSenseManager;
import main.java.com.example.CIC_Android.gps.GPSLoggerServiceManager;
import main.java.com.example.CIC_Android.services.BaseSpiceService;
import main.java.com.example.CIC_Android.services.UpdaterGPSRecieverControl;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.08.13
 * Time: 15:49
 */
public class Application extends android.app.Application {

    private static SpiceManager spiceManager = null;
    private static Context context ;
    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        spiceManager = new SpiceManager(BaseSpiceService.class);

        spiceManager.start(this);

        BugSenseManager.init(getAppContext());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        cleanMemoryAndCaches();
    }

    private void cleanMemoryAndCaches() {
    }

    @Override
    public void onTerminate() {

        GPSLoggerServiceManager.getInstance().shutdown(getAppContext());
        spiceManager.shouldStop();
        super.onTerminate();
    }

    public static Context getAppContext(){
        return context;
    }

    public static SpiceManager getSpiceManager(){
       return spiceManager;
    }

}
