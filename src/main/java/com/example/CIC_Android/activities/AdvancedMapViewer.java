/*
 * Copyright 2010, 2011, 2012 mapsforge.org
 *
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package main.java.com.example.CIC_Android.activities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;
import com.example.CIC_Android.R;
import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.overlay.ArrayCircleOverlay;
import org.mapsforge.android.maps.overlay.OverlayCircle;
import org.mapsforge.core.model.GeoPoint;
import org.mapsforge.map.reader.header.FileOpenResult;

import java.io.File;


public class AdvancedMapViewer extends MapActivity {
//
//    private static final File MAP_FILE = new File(Environment.getExternalStorageDirectory().getPath(), "russia.map");
//    private MapView mapView = null;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        mapView = new MapView(this);
//        mapView.setClickable(true);
//        mapView.setBuiltInZoomControls(true);
//        FileOpenResult fileOpenResult = mapView.setMapFile(MAP_FILE);
//        if (!fileOpenResult.isSuccess()) {
//            Toast.makeText(this, fileOpenResult.getErrorMessage(), Toast.LENGTH_LONG).show();
//            finish();
//        }
//        setContentView(mapView);
//    }

//    /**
//     * @param showToast
//     *            defines whether a toast message is displayed or not.
//     */
//    void disableSnapToLocation(boolean showToast) {
//        if (this.LocationOverlayItem.isSnapToLocationEnabled()) {
//            this.LocationOverlayItem.setSnapToLocationEnabled(false);
//            this.snapToLocationView.setChecked(false);
//            this.mapView.setClickable(true);
//            if (showToast) {
//                showToastOnUiThread(getString(R.string.snap_to_location_disabled));
//            }
//        }
//    }
//
//    /**
//     * @param showToast
//     *            defines whether a toast message is displayed or not.
//     */
//    void enableSnapToLocation(boolean showToast) {
//        if (!this.myLocationOverlay.isSnapToLocationEnabled()) {
//            this.myLocationOverlay.setSnapToLocationEnabled(true);
//            this.mapView.setClickable(false);
//            if (showToast) {
//                showToastOnUiThread(getString(R.string.snap_to_location_enabled));
//            }
//        }
//    }


//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (mapView == null) return;
//
//        gotoLastKnownPosition();
//    }
//
//    private void paintCircle(GeoPoint geoPoint) {
//        Paint paintFill = new Paint(Paint.ANTI_ALIAS_FLAG);
//        paintFill.setStyle(Paint.Style.FILL);
//        paintFill.setColor(Color.BLUE);
//        paintFill.setAlpha(64);
//
//        Paint paintStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
//        paintStroke.setStyle(Paint.Style.STROKE);
//        paintStroke.setColor(Color.BLUE);
//        paintStroke.setAlpha(96);
//        paintStroke.setStrokeWidth(3);
//
//        ArrayCircleOverlay arrayCircleOverlay = new ArrayCircleOverlay(paintFill,null);
//
//        arrayCircleOverlay.addCircle(new OverlayCircle(geoPoint,10,"Я тут!"));
//
//        mapView.getOverlays().add(arrayCircleOverlay);
//    }
//    /**
//     * Centers the map to the last known position as reported by the most accurate location provider. If the last
//     * location is unknown, a toast message is displayed instead.
//     */
//    private void gotoLastKnownPosition() {
//        Location bestLocation = null;
//        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        for (String provider : locationManager.getProviders(true)) {
//            Location currentLocation = locationManager.getLastKnownLocation(provider);
//            if (bestLocation == null || bestLocation.getAccuracy() > currentLocation.getAccuracy()) {
//                bestLocation = currentLocation;
//            }
//        }
//
//        // check if a location has been found
//        if (bestLocation != null) {
//            GeoPoint geoPoint = new GeoPoint(bestLocation.getLatitude(), bestLocation.getLongitude());
//            this.mapView.setCenter(geoPoint);
//            paintCircle(geoPoint);
//        } else {
//            Toast.makeText(this, getString(R.string.error_last_location_unknown),Toast.LENGTH_LONG).show();
//        }
//    }
}
