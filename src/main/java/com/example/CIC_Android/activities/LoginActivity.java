package main.java.com.example.CIC_Android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.example.CIC_Android.R;
import main.java.com.example.CIC_Android.Application;
import main.java.com.example.CIC_Android.Utils.VerificationUtils;
import main.java.com.example.CIC_Android.gps.GPSLoggerServiceManager;
import main.java.com.example.CIC_Android.model.TypeOfShift;
import main.java.com.example.CIC_Android.network.AuthorizerWorker;
import main.java.com.example.CIC_Android.network.ParserError;
import main.java.com.example.CIC_Android.network.ShiftWorker;
import main.java.com.example.CIC_Android.services.UpdaterGPSRecieverControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginActivity extends Activity {

    private static final Logger logger = LoggerFactory.getLogger(LoginActivity.class);
    private EditText edLogin;
    private EditText edPassword;
    private Button btLogin;
    private Button btLogout;
    private Button btCheckStatus;

    private Button btShiftOpen;
    private AuthorizerWorker authorizer;
    private ShiftWorker shiftWorker;
    private Button btShiftClose;
    private Button btMap;
    private Button btStopGPSService;
    private Button btStartGPSService;
    private Button btStartGPSLogging;
    private Button btStopGPSLogging;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logger.debug("Program is started");

        initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        ParserError parserError = new ParserError();
        authorizer = new AuthorizerWorker(this,Application.getSpiceManager(),parserError);
        shiftWorker = new ShiftWorker(this, Application.getSpiceManager(),parserError);
    }

    private void initUI() {
        setContentView(R.layout.login);
        edLogin = (EditText)findViewById(R.id.edlogin);
        edPassword = (EditText)findViewById(R.id.edPassword);

        btLogin = (Button) findViewById(R.id.btLogin);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLoginRequest();
            }
        });

        btLogout = (Button) findViewById(R.id.btLogout);
        btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authorizer.logout();
            }
        });

        btCheckStatus = (Button) findViewById(R.id.btCheck);
        btCheckStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authorizer.isLoggedRequest();
            }
        });

        btShiftOpen = (Button) findViewById(R.id.btShiftOpen);
        btShiftOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shiftWorker.switchToShift(authorizer.getCurrentRegistredUser(),
                        TypeOfShift.open,
                        VerificationUtils.getTabletID());
            }
        });
        btShiftClose = (Button) findViewById(R.id.btShiftClose);
        btShiftClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shiftWorker.switchToShift(authorizer.getCurrentRegistredUser(),
                        TypeOfShift.close,
                        VerificationUtils.getTabletID());
            }
        });

        btStartGPSService = (Button) findViewById(R.id.btStartService);
        btStartGPSService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdaterGPSRecieverControl.startGPSLoggerService(Application.getAppContext());

            }
        });


        btStopGPSService = (Button) findViewById(R.id.btStopService);
        btStopGPSService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdaterGPSRecieverControl.stopGPSLoggerService(Application.getAppContext());
            }
        });

        btStartGPSLogging = (Button) findViewById(R.id.btStartGPSLogging);
        btStartGPSLogging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdaterGPSRecieverControl.startLogging(Application.getAppContext());
            }
        });

        btStopGPSLogging = (Button) findViewById(R.id.btStopGPSLogging);
        btStopGPSLogging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdaterGPSRecieverControl.stopLogging(Application.getAppContext());
            }
        });

        btMap =  (Button) findViewById(R.id.btMap);
        btMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMapActivity();
            }
        });
    }

    private void startMapActivity() {
        startActivity(new Intent(this,WindowFragmentActivity.class));
    }

    private void startLoginRequest(){

        String user=edLogin.getText().toString().trim();
        String password=edPassword.getText().toString().trim();

        authorizer.login(user,password);

    }

}
