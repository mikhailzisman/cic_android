package main.java.com.example.CIC_Android.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.08.13
 * Time: 18:11
 */
public class Settings {

    public final static String ServerDomain = "http://cloudycab.sim-tech.ru";
    public final static int databaseVersion = 1;
    public final static String databaseName = "sample_database2.db";
    public static long RefreshRateInSecond = 10;
}
