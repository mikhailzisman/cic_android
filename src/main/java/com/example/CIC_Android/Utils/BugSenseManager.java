package main.java.com.example.CIC_Android.Utils;

import android.content.Context;
import com.bugsense.trace.BugSenseHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.08.13
 * Time: 16:18
 */
public class BugSenseManager {

    private static BugSenseManager instance = new BugSenseManager();

    private static final String APIKEY = "48625882";
    private static Context context;


    public static void init(Context context) {
        if (context != null)
            BugSenseHandler.initAndStartSession(context, APIKEY);

    }

    public static void flush(){
        if (context!=null)
            BugSenseHandler.flush(context);
    }

    public static void addStringKeyValue(String key, String value){
        if (context!=null)
           BugSenseHandler.addCrashExtraData(key, value);
    }

    public static void addStringMap(HashMap<String,String> map){
        if (context!=null)
            BugSenseHandler.addCrashExtraMap(map);
    }


}
