package main.java.com.example.CIC_Android.adapters;

import main.java.com.example.CIC_Android.model.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.09.13
 * Time: 17:44
 */
public class TempStaticRepository {
    public static List<Order> orderList;
    static{
        orderList = new ArrayList<Order>();
        orderList.add(new Order(1, "Один","Два","30р"));
        orderList.add(new Order(2, "dads","Дasdasва","30р"));
        orderList.add(new Order(3, "Одfdsfsин","Двgfdgdа","30р"));
    }
}
