package main.java.com.example.CIC_Android.adapters;

import android.graphics.Bitmap;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 13.09.13
 * Time: 1:26
 */

public class MenuItemButton {

    public MenuItemButton(){
        this.setBmp(null);
    }
    public MenuItemButton(int menu_item_id, String caption, Bitmap bmp){
        setMenu_item_id(menu_item_id);
        setCaption(caption);
        setBmp(bmp);
    }
    private int menu_id;
    private int menu_item_id;
    private Bitmap bmp;
    private String caption;
    public String getCaption() {
        return caption;
    }
    public void setCaption(String caption) {
        this.caption = caption;
    }
    public Bitmap getBmp() {
        return bmp;
    }
    public void setBmp(Bitmap bmp) {
        this.bmp = bmp;
    }
    public int getMenu_item_id() {
        return menu_item_id;
    }
    public void setMenu_item_id(int menu_item_id) {
        this.menu_item_id = menu_item_id;
    }
    public int getMenu_id() {
        return menu_id;
    }
    public void setMenu_id(int menu_id) {
        this.menu_id = menu_id;
    }


}