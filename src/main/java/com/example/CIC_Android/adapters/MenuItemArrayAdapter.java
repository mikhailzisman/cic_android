package main.java.com.example.CIC_Android.adapters;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 13.09.13
 * Time: 1:24
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.CIC_Android.R;

import java.util.ArrayList;
import java.util.List;

public class MenuItemArrayAdapter extends ArrayAdapter<MenuItemButton> {
    final static String tag = "MenuItemAdapter";
    private List<MenuItemButton> menuItemButtonList = new ArrayList<MenuItemButton>();
    public MenuItemArrayAdapter(Context context, int textViewResourceId,
                                List<MenuItemButton> objects) {
        super(context, textViewResourceId, objects);
        this.menuItemButtonList = objects;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View menuItem = convertView;
        if (menuItem == null) {

            Log.d(tag, "Starting XML Row Inflation ... ");
            LayoutInflater inflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            menuItem = inflater.inflate(R.layout.menuitem, parent, false);
            Log.d(tag, "Successfully completed XML Row Inflation!");
        }
        //Set image for button
        ImageView ib = (ImageView) menuItem.findViewById(R.id.itemImageButton);
        ib.setImageBitmap(getBitmap(position));
        TextView tv = (TextView) menuItem.findViewById(R.id.textTitle);
        tv.setText(getText(position));

        return menuItem;

    }

    private Bitmap getBitmap(int position) {
        return menuItemButtonList.get(position).getBmp();
    }

    private String getText(int position) {
        return menuItemButtonList.get(position).getCaption();
    }
}