package main.java.com.example.CIC_Android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.CIC_Android.R;
import main.java.com.example.CIC_Android.model.Order;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.09.13
 * Time: 16:43
 */
public class OrderItemArrayAdapter extends ArrayAdapter<Order> {
    private Context context;
    private List<Order> orderList;

    public OrderItemArrayAdapter(Context context, int textViewResourceId, List<Order> orderList) {
        super(context, textViewResourceId, orderList);
        this.context = context;
        this.orderList = orderList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.order_item, parent, false);
        }

        TextView tvOrderInfo = (TextView)convertView.findViewById(R.id.order_info);
        TextView tvOrderPrice = (TextView)convertView.findViewById(R.id.order_price);

        tvOrderInfo.setText(orderList.get(position).getAddressFrom()+
                " "+orderList.get(position).getAddressTo());
        tvOrderPrice.setText(orderList.get(position).getPrice());

        return convertView;

    }
}
