package main.java.com.example.CIC_Android.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.octo.android.robospice.SpiceManager;
import main.java.com.example.CIC_Android.Application;
import main.java.com.example.CIC_Android.fragments.MapFragment.MyMapFragment;
import main.java.com.example.CIC_Android.interfaces.IOnTimerUpdater;
import main.java.com.example.CIC_Android.network.MapPlaceUpdaterWorker;
import main.java.com.example.CIC_Android.network.ParserError;
import main.java.com.example.CIC_Android.network.WaypointUpdaterWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 18.09.13
 * Time: 0:13
 */
public class UpdaterReciever extends BroadcastReceiver {

    private static final Logger logger = LoggerFactory.getLogger(UpdaterReciever.class);

    List<IListenerGPSUpdates> listenerGPSUpdatesArrayList = new ArrayList<IListenerGPSUpdates>();
    List<IOnTimerUpdater> mapUpdaterList = new ArrayList<IOnTimerUpdater>();

    @Override
    public void onReceive(Context context, Intent intent) {


        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        init();
        logger.debug("Sync process started");
        sync();
        logger.debug("Sync process finished");

        wl.release();
    }


    private void sync() {

        update();

        notifyListeners();
    }

    private void update() {
        for (IOnTimerUpdater mapUpdater : mapUpdaterList) {
            //Start route request and show on map
            mapUpdater.update();
        }
    }

    private void init() {
        //init mapWorker for Routs etc
        //MapNetworkUpdaterWorker mapUpdater = new MapNetworkUpdaterWorker(Application.getAppContext(), Application.getSpiceManager(), new ParserError());
        //mapUpdater.setMapRepositoryController(getMapController());
        //mapUpdaterList.add(mapUpdater);

        //init mapWorker for currentPlaceMarkUpdater
        MapPlaceUpdaterWorker currentPlaceMarkUpdater = new MapPlaceUpdaterWorker();
        currentPlaceMarkUpdater.setMapController(MyMapFragment.getInstance());
        mapUpdaterList.add(currentPlaceMarkUpdater);

        WaypointUpdaterWorker waypointUpdaterWorker = new WaypointUpdaterWorker(Application.getAppContext(), Application.getSpiceManager(), new ParserError());
        mapUpdaterList.add(waypointUpdaterWorker);
    }


    public void addListener(IListenerGPSUpdates listenerGPSUpdates) {
        if (listenerGPSUpdatesArrayList != null)
            listenerGPSUpdatesArrayList.add(listenerGPSUpdates);
    }

    public void clearListener(IListenerGPSUpdates listenerGPSUpdates) {
        listenerGPSUpdatesArrayList.clear();
    }

    private void notifyListeners() {
        for (IListenerGPSUpdates listenerGPSUpdates : listenerGPSUpdatesArrayList)
            listenerGPSUpdates.OnUpdate();
    }

    public interface IListenerGPSUpdates {
        void OnUpdate();
    }

    private SpiceManager getSpiceManager() {
        return main.java.com.example.CIC_Android.Application.getSpiceManager();
    }
}
