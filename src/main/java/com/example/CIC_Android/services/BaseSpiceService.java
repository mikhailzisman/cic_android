package main.java.com.example.CIC_Android.services;

import android.app.Application;
import com.octo.android.robospice.SpringAndroidSpiceService;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.ormlite.InDatabaseObjectPersisterFactory;
import com.octo.android.robospice.persistence.ormlite.RoboSpiceDatabaseHelper;
import main.java.com.example.CIC_Android.Utils.Settings;
import main.java.com.example.CIC_Android.gps.Waypoint;
import main.java.com.example.CIC_Android.model.*;
import org.codehaus.jackson.map.SerializationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.08.13
 * Time: 18:22
 */
public class
        BaseSpiceService extends SpringAndroidSpiceService {
    private static final Logger logger = LoggerFactory.getLogger(BaseSpiceService.class);
    private static final int WEBSERVICES_TIMEOUT = 10000;

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();

//        // init
//        InFileStringObjectPersister inFileStringObjectPersister = new InFileStringObjectPersister(application);
//        InFileBitmapObjectPersister inFileBitmapObjectPersister = new InFileBitmapObjectPersister(application);
//
//        cacheManager.addPersister(inFileStringObjectPersister);
//        cacheManager.addPersister(inFileBitmapObjectPersister);

        List<Class<?>> classCollection = new ArrayList<Class<?>>();

        // add persisted classes to class collection
        classCollection.add( User.class );
        classCollection.add( LoginResponse.class );
        classCollection.add( LogOutResponse.class );
        classCollection.add( ShiftResponse.class );
        classCollection.add( IsAuthorizedResponse.class );
        classCollection.add( Waypoint.class );
        classCollection.add( WaypointResponse.class );

        // init
        RoboSpiceDatabaseHelper databaseHelper = new RoboSpiceDatabaseHelper( application, Settings.databaseName, Settings.databaseVersion );
        InDatabaseObjectPersisterFactory inDatabaseObjectPersisterFactory = new InDatabaseObjectPersisterFactory( application, databaseHelper, classCollection );

        cacheManager.addPersister( inDatabaseObjectPersisterFactory );

        return cacheManager;
    }

    @Override
    public int getThreadCount() {
        return 3;
    }


    @Override
    public RestTemplate createRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        // set timeout for requests

        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setReadTimeout( WEBSERVICES_TIMEOUT );
        httpRequestFactory.setConnectTimeout( WEBSERVICES_TIMEOUT );
        restTemplate.setRequestFactory( httpRequestFactory );

        // web services support xml responses
        SimpleXmlHttpMessageConverter simpleXmlHttpMessageConverter = new SimpleXmlHttpMessageConverter();
        FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();

        final List<HttpMessageConverter< ? >> listHttpMessageConverters = restTemplate.getMessageConverters();

        List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
        MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        supportedMediaTypes.add(mediaType);
        MappingJacksonHttpMessageConverter jacksonConverter = new MappingJacksonHttpMessageConverter();


        jacksonConverter.setSupportedMediaTypes(supportedMediaTypes);

        listHttpMessageConverters.add( simpleXmlHttpMessageConverter );
        listHttpMessageConverters.add( formHttpMessageConverter );
        listHttpMessageConverters.add( stringHttpMessageConverter );
        listHttpMessageConverters.add( jacksonConverter);
        restTemplate.setMessageConverters( listHttpMessageConverters );
        return restTemplate;
    }
}
