package main.java.com.example.CIC_Android.services;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import main.java.com.example.CIC_Android.Application;
import main.java.com.example.CIC_Android.Utils.Settings;
import main.java.com.example.CIC_Android.gps.GPSLoggerService;
import main.java.com.example.CIC_Android.gps.GPSLoggerServiceManager;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 18.09.13
 * Time: 0:26
 */
public class UpdaterGPSRecieverControl {

    public static void startGPSLoggerService(Context context) {
        //if (!isGPSLoggerServiceRunning(context))
               startService();
    }

    public static void startLogging(Context context) {
        if (isGPSLoggerServiceRunning(context)){
            GPSLoggerServiceManager.getInstance().startGPSLogging(null);
            startRefreshProcess(context, Settings.RefreshRateInSecond);
        }
    }

    public static void stopLogging(Context context) {
        if (isGPSLoggerServiceRunning(context)){
            GPSLoggerServiceManager.getInstance().stopGPSLogging();
            cancelRefreshProcess(context);
        }
    }

    public static void stopGPSLoggerService(Context context) {
        if (isGPSLoggerServiceRunning(context))
            stopService(context);
    }

    private static void stopService(Context context) {
        GPSLoggerServiceManager.getInstance().shutdown(context);
    }

    private static void startService() {
        GPSLoggerServiceManager.getInstance().startup(Application.getAppContext(), new Runnable() {
            @Override
            public void run() {
                Toast.makeText(Application.getAppContext(),"GPS is started",Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void cancelRefreshProcess(Context context) {
        Intent intent = new Intent(context, UpdaterReciever.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        //  | PendingIntent.FLAG_ONE_SHOT
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    public static void startRefreshProcess(Context context, long RefreshRateInSeconds) {

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, UpdaterReciever.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + RefreshRateInSeconds * 1000, RefreshRateInSeconds * 1000, pi); // Millisec * Second * Minute
    }

    private static boolean isGPSLoggerServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (GPSLoggerService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}